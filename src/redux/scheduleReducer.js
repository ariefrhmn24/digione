import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  scheduleList: [],
};

export const scheduleReducer = createSlice({
  name: 'scheduleRdx',
  initialState,
  reducers: {
    saveScheduleList: (state, action) => {
      state.scheduleList = action.payload;
    },
    resetScheduleRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveScheduleList,
  resetScheduleRdx
} = scheduleReducer.actions;

export default scheduleReducer.reducer;