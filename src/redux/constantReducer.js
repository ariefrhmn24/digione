import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  user: {},
  isWalktrough: true,
  headerTitle: '',
  accessToken: '',
  lang: {
    id: 'id',
    desc: 'Indonesia'
  }
};

export const constantReducer = createSlice({
  name: 'constantRdx',
  initialState,
  reducers: {
    saveInitialize: (state, action) => {
      state.user = action.payload.parsedUser;
      state.isWalktrough = action.payload.isWalktrough;
    },
    saveHeaderTitle: (state, action) => {
      state.headerTitle = action.payload;
    },
    saveWalktrough: (state, action) => {
      state.isWalktrough = action.payload;
    },
    saveLang: (state, action) => {
      state.lang = action.payload;
    },
    saveAccessToken (state, action) {
      return {
        ...state,
        accessToken: action.payload
      };
    },
    resetConstantRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveInitialize,
  saveHeaderTitle,
  saveWalktrough,
  saveAccessToken,
  resetConstantRdx,
  saveLang
} = constantReducer.actions;

export default constantReducer.reducer;