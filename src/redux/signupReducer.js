import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  email: '',
  type: '',
  postRegister: {},
  postVerify: {}
};

export const signupReducer = createSlice({
  name: 'signupRdx',
  initialState,
  reducers: {
    saveEmail: (state, action) => {
      state.email = action.payload;
      state.type = 'email';
    },
    savePostRegister: (state, action) => {
      state.postRegister = action.payload;
    },
    savePostVerify: (state, action) => {
      state.postVerify = action.payload;
    },
    resetSignupRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveEmail,
  savePostRegister,
  savePostVerify,
  resetSignupRdx
} = signupReducer.actions;

export default signupReducer.reducer;