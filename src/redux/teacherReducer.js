import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  teacherList: {},
  teacherSelected: {},
  teacherDetail: {}
};

export const teacherReducer = createSlice({
  name: 'teacherRdx',
  initialState,
  reducers: {
    saveTeacherList: (state, action) => {
      state.teacherList = action.payload;
    },
    saveTeacherSelected: (state, action) => {
      state.teacherSelected = action.payload;
    },
    saveTeacherDetail: (state, action) => {
      state.teacherDetail = action.payload;
    },
    resetTeacherRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveTeacherList,
  saveTeacherSelected,
  saveTeacherDetail,
  resetTeacherRdx
} = teacherReducer.actions;

export default teacherReducer.reducer;