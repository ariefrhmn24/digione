import {configureStore} from '@reduxjs/toolkit';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import loginReducer from './loginReducer';
import constantReducer from './constantReducer';
import accountReducer from './accountReducer';
import subjectReducer from './subjectReducer';
import scheduleReducer from './scheduleReducer';
import teacherReducer from './teacherReducer';
import signupReducer from './signupReducer';

const rootReducer = combineReducers({
  loginRdx: loginReducer,
  constantRdx: constantReducer,
  accountRdx: accountReducer,
  subjectRdx: subjectReducer,
  scheduleRdx: scheduleReducer,
  teacherRdx: teacherReducer,
  signupRdx: signupReducer
});

const persistConfig = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
  // blacklist: ['scheduleRdx']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export default store;

// export default configureStore({
//   reducer: {
//     loginRdx: loginReducer,
//     constantRdx: constantReducer,
//     accountRdx: accountReducer,
//     subjectRdx: subjectReducer,
//     scheduleRdx: scheduleReducer
//   },
// });

// export const create = (id) => {
//   return async (dispatch, getState) => {
//        const currentState= getState().example;
//   };
// };