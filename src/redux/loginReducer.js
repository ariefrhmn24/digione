import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  isLogin: false,
  postLogin: {},
};

export const loginReducer = createSlice({
  name: 'loginRdx',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
    saveLogin: (state, action) => {
      state.isLogin = true;
      state.postLogin = action.payload;
    },
    resetLoginRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  increment, 
  decrement, 
  incrementByAmount, 
  saveLogin,
  resetLoginRdx
} = loginReducer.actions;

export default loginReducer.reducer;