import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  subjectList: [],
  subjectSelected: {}
};

export const subjectReducer = createSlice({
  name: 'subjectRdx',
  initialState,
  reducers: {
    saveSubjectList: (state, action) => {
      state.subjectList = action.payload;
    },
    saveSubjectSelected: (state, action) => {
      state.subjectSelected = action.payload;
    },
    resetSubjectRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveSubjectList,
  saveSubjectSelected,
  resetSubjectRdx
} = subjectReducer.actions;

export default subjectReducer.reducer;