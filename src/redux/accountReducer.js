import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  profile: {},
};

export const accountReducer = createSlice({
  name: 'accountRdx',
  initialState,
  reducers: {
    saveProfile: (state, action) => {
      state.profile = action.payload;
    },
    resetAccountRdx (state, action) {
      return {
        ...initialState
      };
    }
  },
});

// Action creators are generated for each case reducer function
export const {
  saveProfile,
  resetAccountRdx
} = accountReducer.actions;

export default accountReducer.reducer;