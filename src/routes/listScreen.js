export const listHome = [
  {
    key: 1,
    name: 'ExplorePage',
    component: require('containers/scenes/Explore/ExplorePage').default,
    options: {
      title: '',
      headerShown: true,
      headerColor: 'blue',
      isBack: false,
      // isEmpty: false
    },
  },
  {
    key: 2,
    name: 'TeacherListPage',
    component: require('containers/scenes/Teacher/TeacherList').default,
    options: {
      title: 'Tutors',
      headerShown: true,
      headerColor: 'blue',
      isBack: true,
    },
  },
  {
    key: 3,
    name: 'TeacherDetailPagez',
    component: require('containers/scenes/Teacher/TeacherDetail').default,
    options: {
      title: 'Tutor Details',
      headerShown: true,
      headerColor: 'blue',
      isBack: true,
    },
  },
];

export const listClass = [
  {
    key: 31,
    name: 'ClassPage',
    component: require('containers/scenes/Class/ClassPage').default,
    options: {
      title: 'My Classes',
      headerShown: true,
      headerColor: 'blue',
      isBack: false,
    },
  },
  {
    key: 32,
    name: 'SubjectDetailsPage',
    component: require('containers/scenes/Class/SubjectDetails').default,
    options: {
      title: '',
      headerShown: true,
      headerColor: 'blue',
      isBack: true
    },
  },
];

export const listAccount = [
  {
    key: 1,
    name: 'AccountPage',
    component: require('containers/scenes/Account/AccountPage').default,
    options: {
      title: 'Account',
      headerShown: true,
      headerColor: 'blue',
      isBack: false,
    },
  },
  {
    key: 2,
    name: 'LanguagesPage',
    component: require('containers/scenes/Account/LanguagesPage').default,
    options: {
      title: 'txtLanguage',
      headerShown: true,
      headerColor: 'blue',
      isBack: true
    },
  },
];

export const listModal = [
  {
    key: 1,
    name: 'LoginPage',
    component: require('containers/scenes/SignIn/LoginPage').default,
    options: {
      title: 'Login',
      headerShown: true,
      isBack: true
    },
  },
  {
    key: 2,
    name: 'SignupEmailPage',
    component: require('containers/scenes/Authorization/SignupEmail').default,
    options: {
      title: '',
      headerShown: true,
      isBack: true
    },
  },
  {
    key: 3,
    name: 'SignupOtpPage',
    component: require('containers/scenes/Authorization/SignupOtp').default,
    options: {
      title: '',
      headerShown: true,
      isBack: true
    },
  },
  {
    key: 4,
    name: 'PasswordPage',
    component: require('containers/scenes/Authorization/PasswordPage').default,
    options: {
      title: '',
      headerShown: true,
      isBack: false,
      gestureEnabled: false
    },
  },
  {
    key: 5,
    name: 'SignupIdPage',
    component: require('containers/scenes/Authorization/SignupId').default,
    options: {
      title: '',
      headerShown: true,
      isBack: true
    },
  },
  {
    key: 6,
    name: 'SignupCompletedPage',
    component: require('containers/scenes/Authorization/SignupCompleted').default,
    options: {
      title: '',
      headerShown: false,
    },
  },
];