/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/display-name */
import React, {useEffect, useState} from 'react';
import {StyleSheet, Easing} from 'react-native';
import {NavigationContainer, useNavigation, StackActions} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createStackNavigator, CardStyleInterpolators} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {isNil, result} from 'lodash';

import {ownColor} from 'theme/color';
import HeaderBlue from 'components/molecules/HeaderBlue';
import HeaderWhite from 'components/molecules/HeaderWhite';
import {listHome, listClass, listAccount, listModal} from './listScreen';
import {AuthContext} from './context';
import {navigationRef} from './navigationService';
import IcChevronLeft from 'assets/svg/ic-chevron-left.svg';
import IcTabHome from 'assets/svg/ic-tab-home.svg';
import IcTabHomeActive from 'assets/svg/ic-tab-home-active.svg';
import IcClass from 'assets/svg/ic-class.svg';
import IcClassActive from 'assets/svg/ic-class-active.svg';
import IcProfile from 'assets/svg/ic-profile.svg';
import IcProfileActive from 'assets/svg/ic-profile-active.svg';

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 100,
    mass: 3,
    overshootClamping: false,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  }
};

const closeConfig = {
  animation: 'timing',
  config: {
    duration: 250,
    easing: Easing.linear,
  }
};

// const ClassStack = createNativeStackNavigator();
const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const ClassStack = createStackNavigator();
const AccountStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const onPressHeader = ({navigation}) => () => {
  navigation.pop();
};

const HomeStackScreen = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerShown: true
    }}
  >
    {listHome.map((i) => (
      <HomeStack.Screen
        key={i.key}
        name={i.name}
        component={i.component}
        options={({route, navigation}) => ({
          header: ({navigation, route, options, back}) => { 
            if (i.options.headerColor === 'blue') {
              return ( 
                <HeaderBlue 
                  title={i.options.title} 
                  isBack={i.options.isBack} 
                  isEmpty={i.options.isEmpty}
                  onPressHeader={onPressHeader({navigation})} />
              );
            } else {
              return <HeaderWhite 
                title={i.options.title} 
                isBack={i.options.isBack} 
                isEmpty={i.options.isEmpty}
                onPressHeader={onPressHeader({navigation})} />; 
            }
          },
          headerShown: i.options.headerShown,
          headerStyle: styles.headerStyle,
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          transitionSpec: {
            open: config,
            close: closeConfig,
          },
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        })}
        
      />
    ))}
  </HomeStack.Navigator>
);

const ClassStackScreen = () => (
  <ClassStack.Navigator>
    {listClass.map((i) => (
      <ClassStack.Screen
        key={i.key}
        name={i.name}
        component={i.component}
        options={{
          header: ({navigation, route, options, back}) => { 
            if (i.options.headerColor === 'blue') {
              return <HeaderBlue 
                title={i.options.title} 
                isBack={i.options.isBack} 
                isEmpty={i.options.isEmpty}
                onPressHeader={onPressHeader({navigation})} />; 
            } else {
              return <HeaderWhite 
                title={i.options.title} 
                isBack={i.options.isBack} 
                isEmpty={i.options.isEmpty}
                onPressHeader={onPressHeader({navigation})} />; 
            }
          },
          headerShown: i.options.headerShown,
          headerStyle: styles.headerStyle,
          headerTintColor: 'white',
        }}
      />
    ))}
  </ClassStack.Navigator>
);

const AccountStackScreen = () => (
  <AccountStack.Navigator>
    {listAccount.map((i) => (
      <AccountStack.Screen
        key={i.key}
        name={i.name}
        component={i.component}
        options={{
          header: ({navigation, route, options, goBack}) => { 
            if (i.options.headerColor === 'blue') {
              return ( 
                <HeaderBlue 
                  title={i.options.title} 
                  isBack={i.options.isBack} 
                  isEmpty={i.options.isEmpty}
                  onPressHeader={onPressHeader({navigation})} />
              );
            } else {
              return <HeaderWhite 
                title={i.options.title} 
                isBack={i.options.isBack} 
                isEmpty={i.options.isEmpty}
                onPressHeader={onPressHeader({navigation})} />; 
            }
          },
          headerShown: i.options.headerShown,
          headerStyle: styles.headerStyle,
        }}
      />
    ))}
  </AccountStack.Navigator>
);

const TabScreen = () => (
  <Tabs.Navigator
  // tabBar={(props) => <MyTabBar {...props} />} 
    screenOptions={{
      headerShown: false,
      tabBarLabelStyle: {
        color: ownColor.primary,
      },
      tabBarStyle: styles.tabBarStyle
    }}
  >
    <Tabs.Screen 
      key={1}
      name='HomeTab' 
      component={HomeStackScreen} 
      options={{
        headerShown: false,
        title: 'Explore',
        tabBarIcon: ({focused}) => focused ? <IcTabHomeActive /> : <IcTabHome />,
      }}
    />
    <Tabs.Screen 
      key={2}
      name='SearchTab' 
      component={ClassStackScreen}
      options={{
        headerShown: false,
        title: 'My Class',
        tabBarIcon: ({focused}) => focused ? <IcClassActive /> : <IcClass fill='#808CAD' />,
      }}/>
    <Tabs.Screen 
      key={3}
      name='ProfileTab' 
      component={AccountStackScreen}
      options={{
        headerShown: false,
        title: 'Account',
        tabBarIcon: ({focused}) => focused ? <IcProfileActive /> : <IcProfile />,
      }}/>
  </Tabs.Navigator>
);

function Routes() {
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const constantRdx = useSelector((state) => state.constantRdx);
  const [rdxLang] = useState(result(constantRdx, 'lang.id', 'id'));

  const authContext = React.useMemo(() => ({
    signIn: () => {
      // setIsLogin(true);
    }
  }), []);

  useEffect(() => {
    async function initialize() {
      onLanguage();
      setTimeout(() => {
        SplashScreen.hide();
      }, 250);
      // getAllKeys();
    }

    initialize();
  }, []);

  const onLanguage = async () => {
    if (rdxLang === 'id') {
      i18n.changeLanguage('id');
    } else {
      i18n.changeLanguage('en');
    }
  };
  
  // const getAllKeys = async () => {
  //   let keys = [];
  //   try {
  //     keys = await AsyncStorage.getAllKeys();
  //   } catch (e) {
  //     // read key error
  //   }
  //   console.log(keys);
  // };

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer 
        ref={navigationRef}
      >
        <Stack.Navigator 
          screenOptions={{
            // animation: 'slide_from_right',
            // headerShown: false,
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0
            },
          }}>
          <Stack.Group >
            <Stack.Screen
              key={2}
              name='XpertTab'
              component={TabScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              key={3}
              name='WalktroughPage'
              component={require('containers/scenes/Walktrough/WalktroughPage').default}
              options={{
                headerShown: false,
                tabBarStyle: {
                  display: 'none'
                }
              }}
            />
          </Stack.Group>
          
          <Stack.Group
            screenOptions={({route, navigation}) => ({
              gestureEnabled: true,
              gestureDirection: 'vertical',
              transitionSpec: {
                open: config,
                close: closeConfig,
              },
              cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
            })}>
            {listModal.map((i) => (
              <Stack.Screen
                key={i.key}
                name={i.name}
                component={i.component}
                options={{
                  header: ({navigation, route, options, back}) => { 
                    if (i.options.headerColor === 'blue') {
                      return ( 
                        <HeaderBlue 
                          title={i.options.title} 
                          isBack={i.options.isBack} 
                          isEmpty={i.options.isEmpty}
                          onPressHeader={onPressHeader({navigation})} />
                      );
                    } else {
                      return <HeaderWhite 
                        title={i.options.title} 
                        isBack={i.options.isBack} 
                        onPressHeader={onPressHeader({navigation})} />; 
                    }
                  },
                  headerShown: i.options.headerShown,
                  gestureEnabled: i.options.gestureEnabled,
                  headerStyle: styles.headerStyle,
                }}
              />
            ))}
          </Stack.Group>
          
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

export default Routes;

const styles = StyleSheet.create({
  textWhite: {
    color: 'white',
  },
  headerStyle: {
    backgroundColor: 'white',
  },
  tabBarStyle: { 
    // paddingTop: 10
  }
});
