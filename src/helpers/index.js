import {REACT_APP_API_KEY} from '@env';
// import AES from 'crypto-js/aes';
import CryptoJS from 'crypto-js';
// import base64 from 'react-native-base64';

export const hideCharacters = (char) => {
  const numstring = char.substring(4, 7);
  const result = char.replace(numstring, '***');
  return result;
};

export const getKeyYoutube = (link) => {
  const videoID = link.split('v=')[1];
  return videoID;
};

export const regexMail = () => {
  const result = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return result;
};

export const maskingEmail = (char) => {
  const result = char.replace(/^(.)(.*)(.@.*)$/,
    (_, a, b, c) => a + b.replace(/./g, '*') + c
  );
  return result;
};

export const doEncrypt = (pass) => {
  // let decodeBase64 = base64.decode(REACT_APP_API_KEY);
  let cypherText = CryptoJS.AES.encrypt(pass, REACT_APP_API_KEY).toString();

  return cypherText;
};

export const uniqueID = () => {
  const txt = 'xxxxxxxxxxxxxxx';
  const uuid = txt.replace(/[xy]/g, (c) => {
    let r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString();
  });
  return uuid;
};

export const validateDate = (date) => {
  const regexDate = new RegExp(
    /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
  );
  const result = regexDate.test(date);
  return result; // true or false
};

export const limitText = (text, length) => {
  let result = '';
  if (text.length > length) result = `${text.substring(0, length)} ....`;
  else result = text;
  return result;
};

// U can use these function if redux-persist is not persisted
// export const saveToAsync = async (key: string, data: any) => {
//   const strKey = String(key);
//   const strData =
//     data instanceof Object || data instanceof Array
//       ? JSON.stringify(data)
//       : data;
//   await AsyncStorage.setItem(strKey, strData);
//   return;
// };

// export const getFromAsync = async (key: string) => {
//   const data = await AsyncStorage.getItem(key);
//   return data;
// };

export const localeDate = () => ({
  monthNames: [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  ],
  monthNamesShort: [
    'Jan.',
    'Feb.',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul.',
    'Agu',
    'Sep.',
    'Okt.',
    'Nov.',
    'Des.',
  ],
  dayNames: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
  dayNamesShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
});
