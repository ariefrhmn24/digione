export const ownColor = {
  primary: '#0051d3',
  primary10: '#B9CEF3',
  primary100: '#618fe5',
  primary500: '#0051d3',
  
  secondary: '#ff7100',
  secondary500: '#ff7100',

  bg500: '#F2F4F7',

  text500: '#3B4B74',
  text100: '#afafaf',

  red: '#86001A',
  disableRed: 'rgba(134, 0, 26, 0.5)',
  roseRed: '#FBDADB',
  white: '#FFFFFF',
  grey: '#C2C2C2',
  lightGrey: '#C5C5C5',
  black: '#222222',
  dark: '#000000',
  darkGrey: '#1F2937',
  darkTransparent: 'rgba(0,0,0,0.7)',
  yellow: '#FFFFB0',
  yellowBold: '#CAB350',
  green: '#10B981',
  darkGreen: '#047857',
  blue: '#1976d2'
};
