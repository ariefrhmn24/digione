import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

export const ownSize = {
  '2xs': RFValue(8),
  xs: RFValue(10),
  sm: RFValue(12),
  md: RFValue(14),
  lg: RFValue(16),
  xl: RFValue(18),
  '2xl': RFValue(20),
  '3xl': RFValue(24),
  '4xl': RFValue(30),
  '5xl': RFValue(36),
  '6xl': RFValue(48),
  '7xl': RFValue(60),
  '8xl': RFValue(72),
  '9xl': RFValue(96),
};
