import {NativeBaseProvider, extendTheme} from 'native-base';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import {Platform} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

export const colorModeManager = {
  get: async () => {
    try {
      // let val = await AsyncStorage.getItem('@my-app-color-mode');
      let val = '';
      return val === 'dark' ? 'dark' : 'light';
    } catch (e) {
      return 'light';
    }
  },
  set: async (value) => {
    try {
      // await AsyncStorage.setItem('@my-app-color-mode', value);
      let val = '';
    } catch (e) {
      return 'light';
    }
  },
};

export function mode(light, dark) {
  return (props) => (props.colorMode === 'dark' ? dark : light);
}

const baseStyle = (props) => {
  const {primary} = props.theme.colors;
  return {
    fontFamily: 'body',
    px: 4,
    py: 2,
    borderRadius: '10',
    color: mode('text.500', 'white')(props),
    placeholderTextColor: mode('muted.400', 'muted.500')(props),
    // background: 'red',
    borderColor: mode('muted.200', 'muted.600')(props),
    _disabled: {
      opacity: 0.8,
      bg: mode('muted.100', 'muted.700')(props),
    },
    _hover: {
      borderColor: mode('muted.300', 'muted.500')(props),
    },
    _invalid: {
      borderColor: mode('error.600', 'error.200')(props),
    },
    _focus: {
      // style: {...focusRing},
      borderColor: 'secondary.500',
    },
    _android: {
      px: 3,
      py: 3,
      fontSize: 'md',
      _focus: {
        borderColor: 'secondary.500',
      },
    },
    _ios: {
      px: 4,
      py: 4,
      fontSize: 'md',
      _focus: {
        borderColor: 'secondary.500',
      },
    },
    _web: {
      outlineWidth: 0,
      lineHeight: 'lg', // Todo: Move to _web inside size so that sm and xs don't have this much height
    },
  };
};

const config = {
  useSystemColorMode: false,
  initialColorMode: 'light'
};

const components = {
  Input: {
    baseStyle,
    defaultProps: {},
    variants: {},
    sizes: {},
  },
  Text: {
    baseStyle: {
      color: 'text.500',
      fontWeight: '400',
      fontFamily: 'body',
      fontStyle: 'normal',
      fontSize: 'sm',
      letterSpacing: 'md',
      lineHeight: 'lg',
    },
    defaultProps: {
      color: 'text.500',
      // size: 'sm'
      fontSize: 'sm'
    },
    variants: {},
    sizes: {
      '3xl': {
        fontSize: RFValue(24)
      },
      '2xl': {
        fontSize: RFValue(20)
      },
      xl: {
        fontSize: RFValue(18)
      },
      lg: {
        fontSize: RFValue(16)
      },
      md: {
        fontSize: RFValue(14)
      },
      sm: {
        fontSize: RFValue(12)
      },
      xs: {
        fontSize: RFValue(10)
      }
    }
  },
  Button: {
    // Can simply pass default props to change default behaviour of components.
    baseStyle: {
      // rounded: 'md',
      background: 'primary.500',
      borderRadius: '10',
      shadow: '2'
    },
    defaultProps: {
      background: 'primary.500',
      borderRadius: '10',
      shadow: '2'
    },
    variants: {
      secondary: ({
        colorScheme
      }) => ({
        bg: 'secondary.500',
        // rounded: "full"
      }),
      submitted: ({colorScheme}) => ({
        bg: 'brand.500',
        py: 4,
        size: 'lg'
      })
    }
  },
};

const colors = {
  primary: {
    10: '#B9CEF3',
    50: '#84A8EB',
    100: '#618fe5',
    200: '#5083e2',
    300: '#3e77e0',
    400: '#2c6add',
    500: '#0051d3',
    600: '#1f58c1',
    700: '#1d50af',
    800: '#1a489e',
    900: '#17408c',
    1000: '#14387b',
  },
  secondary: {
    500: '#FF7100'
  },
  brand: {
    500: '#0051d3'
  },
  text: {
    500: '#3B4B74',
    100: '#afafaf'
  },
  bg: {
    500: '#F2F4F7'
  }
};

const fontConfig = {
  Roboto: {
    100: {
      normal: 'Roboto-Light',
      italic: 'Roboto-LightItalic',
    },
    200: {
      normal: 'Roboto-Light',
      italic: 'Roboto-LightItalic',
    },
    300: {
      normal: 'Roboto-Light',
      italic: 'Roboto-LightItalic',
    },
    400: {
      normal: 'Roboto-Regular',
      italic: 'Roboto-Italic',
    },
    500: {
      normal: 'Roboto-Medium',
    },
    600: {
      normal: 'Roboto-Medium',
      italic: 'Roboto-MediumItalic',
    },
    700: {
      normal: 'Roboto-Bold',
    },
    800: {
      normal: 'Roboto-Bold',
      italic: 'Roboto-BoldItalic',
    },
    900: {
      normal: 'Roboto-Bold',
      italic: 'Roboto-BoldItalic',
    },
  },
};

const fonts = {
  heading: 'Roboto',
  body: 'Roboto',
  mono: 'Roboto',
};

const fontSizes = {
  '2xs': RFValue(8),
  xs: RFValue(10),
  sm: RFValue(12),
  md: RFValue(14),
  lg: RFValue(16),
  xl: RFValue(18),
  '2xl': RFValue(20),
  '3xl': RFValue(24),
  '4xl': RFValue(30),
  '5xl': RFValue(36),
  '6xl': RFValue(48),
  '7xl': RFValue(60),
  '8xl': RFValue(72),
  '9xl': RFValue(96),
};

const typography = {
  letterSpacings: {
    xs: '-0.05em',
    sm: '-0.025em',
    md: 0,
    lg: '0.025em',
    xl: '0.05em',
    '2xl': '0.1em',
  },
  lineHeights: {
    '2xs': '1em',
    xs: '1.125em',
    sm: '1.25em',
    md: '1.375em',
    lg: '1.5em',
    xl: '1.75em',
    '2xl': '2em',
    '3xl': '2.5em',
    '4xl': '3em',
    '5xl': '4em',
  },
  fontWeights: {
    hairline: 100,
    thin: 200,
    light: 300,
    normal: 400,
    medium: 500,
    semibold: 600,
    bold: 700,
    extrabold: 800,
    black: 900,
    extrablack: 950,
  }
};

export const theme = extendTheme({components, config, colors, fontConfig, fonts, typography, fontSizes});