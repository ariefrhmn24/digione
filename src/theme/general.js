import {StyleSheet} from 'react-native';
import {ownColor} from 'theme/color';
import {ownSize} from 'theme/size';
import {ownFonts} from 'theme/fonts';

export default StyleSheet.create({
  container_outer: {
    padding: 16,
    flex: 1,
    backgroundColor: ownColor.white
  },
});
