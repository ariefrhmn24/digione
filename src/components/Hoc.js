import React, {ReactNode, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  StyleProp,
  ViewStyle,
  StatusBar, 
  StatusBarProps
} from 'react-native';
import {View, Box} from 'native-base';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';

function FocusAwareStatusBar (props) {
  const isFocused = useIsFocused();

  return isFocused ? <StatusBar {...props} /> : null;
}

function Hoc ({
  spinner,
  children,
  style,
  disable,
  header,
}) {
  // Props
  const backgroundStatusBar = () => {
    if (header === 'blue') return '#1d50af';
    else if (header === 'light') return 'white';
    else if (header === 'dark') return 'cyan';
  };

  return (
    <Box flex='1'>
      <FocusAwareStatusBar
        barStyle={header === 'blue' ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStatusBar()}
      />
      {children}
      {disable && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContent]} />
      )}
      {spinner && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContent]}>
          <ActivityIndicator size='large' color={'blue'} />
        </View>
      )}
    </Box>
  );
}

export default Hoc;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loadingContent: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    justifyContent: 'center',
  },
});
