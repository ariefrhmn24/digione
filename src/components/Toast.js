/* eslint-disable react/display-name */
import React, {createContext, useContext} from 'react';
import {Box, useToast, Text} from 'native-base';

const defaultMessage = 'Please try again later';

const initialState = {
  message: '',
};

export const toastContext = createContext(initialState);

const {Provider} = toastContext;

const ToastProvider = (props) => {
  const toast = useToast();

  const onToast = (message = defaultMessage) => (
    toast.show({
      render: () => (
        <Box bg='blueGray.200' px={4} py={3} rounded='20' mb={10}>
          <Text>{message}</Text>
        </Box>
      ),
    })
  );

  return (
    <>
      <Provider value={onToast}>{props.children}</Provider>
    </>
  );
};

const myToast = () => {
  const toast = useContext(toastContext);

  return toast;
};

export {ToastProvider, myToast};
