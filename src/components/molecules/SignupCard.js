import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView, Button} from 'native-base';
import {useTranslation} from 'react-i18next';

import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import IcBgSignUp from 'assets/svg/bg-signup.svg';
import IcLightbulb from 'assets/svg/ic-lightbulb.svg';

export default function SignupCard ({
  originalHeight, 
  originalWidth, 
  title, 
  description,
  onPress
}) {
  const {t, i18n} = useTranslation();

  return (
    <Box bg='#E5EEFB' borderRadius='10' shadow='2'>
      <IcBgSignUp 
        height='100%' 
        preserveAspectRatio='none'
        width='100%' 
        viewBox={`0 0 ${originalWidth} ${originalHeight}`}
        style={{position: 'absolute', bottom: 0}} 
      />
      {/* <Image 
        size='md' 
        resizeMode='contain' 
        source={require('assets/img/bg-signup.png')}
        style={{
          position: 'absolute',
          bottom: '-18%',
          width: '100%'
        }}
        alt='Sign Up'/> */}
      <HStack p='3' justifyContent='space-between' alignItems={'center'}>
        <Box>
          <IcLightbulb width='25' height='25' />
        </Box>
        <VStack>
          <Text bold>{title}</Text>
          <Text fontSize='sm'>{description}</Text>
        </VStack>
        <Button onPress={onPress}>
          {t('txtSignup')}
        </Button>
      </HStack>
    </Box>
  );
}

SignupCard.defaultProps = {
  originalHeight: '100',
  originalWidth: '100',
  title: '',
  description: '',
  onPress: () => {},
};

SignupCard.propTypes = {
  originalHeight: PropTypes.string,
  originalWidth: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
};