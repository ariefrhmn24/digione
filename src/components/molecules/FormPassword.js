/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Text, View, HStack} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import IcRequired from 'assets/svg/ic-required.svg';

import {postSetPassword} from 'services/onboard.thunks';
import {ownColor} from 'theme/color';

export default function FormPassword({
  control, 
  errors, 
  setValue, 
  error, 
  setError, 
  show, 
  show2, 
  handleShow, 
  handleShow2,
  watchPassword
}) {
  const {t, i18n} = useTranslation();
  const enableColor = 'text.500';
  const disableColor = 'text.100';
  const [matchLength, setMatchLength] = useState(disableColor);
  const [matchUpper, setMatchUpper] = useState(disableColor);
  const [matchLower, setMatchLower] = useState(disableColor);
  const [matchNumber, setMatchNumber] = useState(disableColor);
  const [matchSpecial, setMatchSpecial] = useState(disableColor);

  useEffect(() => {
    const upperCaseLetters = new RegExp(/[A-Z]/g);
    const lowerCaseLetters = new RegExp(/[a-z]/g);
    const numbers = new RegExp(/[0-9]/g);
    const special = new RegExp(/[!@#$%^&*(),.?":{}|<>]/g);
    
    if (watchPassword.length >= 8) {
      setMatchLength(enableColor);
    } else {
      setMatchLength(disableColor);
    }
    if (upperCaseLetters.test(watchPassword)) {
      setMatchUpper(enableColor);
    } else {
      setMatchUpper(disableColor);
    }
    if (lowerCaseLetters.test(watchPassword)) {
      setMatchLower(enableColor);
    } else {
      setMatchLower(disableColor);
    }
    if (numbers.test(watchPassword)) {
      setMatchNumber(enableColor);
    } else {
      setMatchNumber(disableColor);
    }
    if (special.test(watchPassword)) {
      setMatchSpecial(enableColor);
    } else {
      setMatchSpecial(disableColor);
    }
  }, [watchPassword]);

  return (
    <VStack space={5}>
      <FormControl isInvalid={'password' in errors}>
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <Input
              type={show ? 'text' : 'password'}
              onBlur={onBlur}
              placeholder={t('txtPassPlaceholder')}
              onChangeText={(val) => onChange(val)}
              value={value}
              InputRightElement={
                <Button bg='none' shadow='none' variant='unstyled' onPress={handleShow}>
                  {show ? <IcEyeOpen /> : <IcEyeOpen />}
                </Button>
              }
              py={4}
              borderRadius={10}
            />
          )}
          name='password'
          rules={{
            required: 'Field is required',
            minLength: 3,
            validate: () => (
              (matchLength === enableColor) && 
              ((matchUpper === enableColor) && (matchLower === enableColor)) && 
              (matchNumber === enableColor) && 
              (matchSpecial === enableColor)
            )
          }}
          defaultValue=''
        />
        {'password' in errors ? 
          <FormControl.ErrorMessage>{t('txtEmailError')}</FormControl.ErrorMessage> 
          :
          <FormControl.HelperText>
            {''}
          </FormControl.HelperText>}
      </FormControl>
      <HStack justifyContent='space-evenly'>
        <VStack space='3'>
          <HStack space='2' alignItems={'center'}>
            <IcRequired fill={matchLength === enableColor ? ownColor.text500 : ownColor.text100} />
            <Text color={matchLength}>{t('txtPassReq1')}</Text>
          </HStack>
          <HStack space='2' alignItems={'center'}>
            <IcRequired fill={matchNumber === enableColor ? ownColor.text500 : ownColor.text100} />
            <Text color={matchNumber}>{t('txtPassReq2')}</Text>
          </HStack>
        </VStack>
        <VStack space='3'>
          <HStack space='2' alignItems={'center'}>
            <IcRequired fill={((matchUpper === enableColor) && (matchLower === enableColor)) ? ownColor.text500 : ownColor.text100} />
            <Text color={((matchUpper === enableColor) && (matchLower === enableColor)) ? enableColor : disableColor}>{t('txtPassReq3')}</Text>
          </HStack>
          
          <HStack space='2' alignItems={'center'}>
            <IcRequired fill={matchSpecial === enableColor ? ownColor.text500 : ownColor.text100} />
            <Text color={matchSpecial}>{t('txtPassReq4')}</Text>
          </HStack>
        </VStack>
      </HStack>
      <FormControl isInvalid={'confirmPassword' in errors}>
        <Controller
          control={control}
          render={({onChange, onBlur, value}) => (
            <Input
              type={show2 ? 'text' : 'password'}
              onBlur={onBlur}
              placeholder={t('txtPassPlaceholder2')}
              onChangeText={(val) => onChange(val)}
              value={value}
              InputRightElement={
                <Button bg='none' shadow='none' variant='unstyled' onPress={handleShow2}>
                  {show2 ? <IcEyeOpen /> : <IcEyeOpen />}
                </Button>
              }
              py={4}
              borderRadius={10}
            />
          )}
          name='confirmPassword'
          rules={{required: 'Field is required', minLength: 3}}
          defaultValue=''
        />
        { error ? <Text mt={4} fontSize='xs' color='red.400'>{error}</Text> : <View />}
        {/* {'confirmPassword' in errors ? 
            <FormControl.ErrorMessage>{t('txtEmailError')}</FormControl.ErrorMessage> 
            :
            <FormControl.HelperText>
              {''}
            </FormControl.HelperText>} */}
      </FormControl>
    </VStack>
  );
}

FormPassword.defaultProps = {
  control: () => {},
  errors: () => {}, 
  setValue: () => {}, 
  error: '', 
  setError: () => {}, 
  show: false, 
  show2: false, 
  handleShow: () => {}, 
  handleShow2: () => {},
  watchPassword: ''
};

FormPassword.propTypes = {
  control: PropTypes.func,
  errors: PropTypes.func,
  setValue: PropTypes.func,
  error: PropTypes.string,
  setError: PropTypes.func,
  show: PropTypes.bool,
  show2: PropTypes.bool,
  handleShow: PropTypes.func,
  handleShow2: PropTypes.func,
  watchPassword: PropTypes.string,
};