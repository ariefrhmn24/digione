// export default function NewsSection () {
//   const [categoryList] = useState([
//     {key: 'all', title: 'All'},
//     {key: 'announcement', title: 'Announcement'},
//     {key: 'news', title: 'News'},
//   ]);
//   const [categorySelected, setCategorySelected] = useState('all');

//   const onSelectedSchedule = ({item, index}) => () => {
//     setCategorySelected(item.key);
//   };

//   return (
//     <Box>
//       <HStack m='5' justifyContent='space-between' alignItems='center'>
//         <Text fontSize='lg' fontWeight='semibold'>{'News & Knowledge'}</Text>
//         <Text>{'See all'}</Text>
//       </HStack>
//       <Box mx='5'>
//         <FilterHorizontal 
//           categoryList={categoryList} 
//           categorySelected={categorySelected} 
//           onSelectedSchedule={onSelectedSchedule} />
//       </Box>
//       <Box>
//         <ScrollView bounces={true} horizontal={true} showsHorizontalScrollIndicator={false}>
//           <HStack m='5' space='5'>
//             <VStack shadow='2' bg='white' borderRadius='10'>
//               <Image
//                 width='240'
//                 borderTopRadius='10'
//                 resizeMode='contain'
//                 source={require('assets/img/dummy-news-1.png')}
//                 alt={'News'} />
//               <HStack p='2' justifyContent='space-between'>
//                 <VStack>
//                   <Text>{'Dilema pembelajaran daring'}</Text>
//                   <Text color='blueGray.400'>{'13 Oktober 2021'}</Text>
//                 </VStack>
//                 <Box alignSelf='center'>
//                   <IcChevronRight width='25' height='25' />
//                 </Box>
//               </HStack>
//             </VStack>
//             <VStack shadow='2' bg='white' borderRadius='10'>
//               <Image
//                 width='240'
//                 borderTopRadius='10'
//                 resizeMode='contain'
//                 source={require('assets/img/dummy-news-1.png')}
//                 alt={'News'} />
//               <HStack p='2' justifyContent='space-between'>
//                 <VStack>
//                   <Text>{'Dilema pembelajaran daring'}</Text>
//                   <Text color='blueGray.400'>{'13 Oktober 2021'}</Text>
//                 </VStack>
//                 <Box alignSelf='center'>
//                   <IcChevronRight width='25' height='25' />
//                 </Box>
//               </HStack>
//             </VStack>
//           </HStack>
//         </ScrollView>
//       </Box>
//     </Box>
//   );
// }
