import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {View, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';

import IcChevronRight from 'assets/svg/ic-chevron-right.svg';

export default function TaskCard ({subjectDate, subjectTime}) {
  // let initials = subjectDate.split(' ');

  return (
    <Pressable>
      <HStack 
        p='5' shadow='2'
        justifyContent='space-between' 
        bg='white' borderRadius='10'
      >
        <VStack space='2'>
          <Text fontSize='lg'>Biology</Text>
          <VStack>
            <Text>Task</Text>
            <Text fontSize='md'>Bab 4 - xxx</Text>
          </VStack>
          <VStack>
            <Text>Deadline</Text>
            <Text fontSize='md'>13 Sep 2021</Text>
          </VStack>
        </VStack>
        <VStack justifyContent='space-between'>
          {/* <IcChevronRight width='25' height='25' /> */}
          <View />
          <IcChevronRight width='25' height='25' />
        </VStack>
      </HStack>
    </Pressable>
  );
}

TaskCard.defaultProps = {
  subjectDate: '',
  subjectTime: '',
};

TaskCard.propTypes = {
  subjectDate: PropTypes.string,
  subjectTime: PropTypes.string,
};