import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';


export default function UpcomingCard ({subjectDate, subjectTime}) {
  const {t, i18n} = useTranslation();

  return (
    <Box flex='1'>
      <ScrollView bounces={true} horizontal={true} showsHorizontalScrollIndicator={false}>
        <HStack m='5' space='5'>
          <VStack width='200' p='5' shadow='1' bg='white' borderRadius='10'>
            <Text fontSize='lg'>{subjectDate}</Text>
            <Divider width='100%' p='2' bg='white' />
            <Text>{t('Schedule')}</Text>
            <Text fontSize='md'>{subjectTime}</Text>
          </VStack>
          <VStack width='200' p='5' shadow='1' bg='white' borderRadius='10'>
            <Text fontSize='lg'>{subjectDate}</Text>
            <Divider width='100%' p='2' bg='white' />
            <Text>{t('Schedule')}</Text>
            <Text fontSize='md'>{subjectTime}</Text>
          </VStack>
        </HStack>
      </ScrollView>
    </Box>
  );
}

UpcomingCard.defaultProps = {
  subjectDate: 'Tue, 21 Sep',
  subjectTime: '09.00 - 09.30',
};

UpcomingCard.propTypes = {
  subjectDate: PropTypes.string,
  subjectTime: PropTypes.string,
};