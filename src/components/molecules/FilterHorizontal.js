import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, FlatList, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';
import {debounce, result} from 'lodash';

import {ownColor} from 'theme/color';
import IcPencil from 'assets/svg/ic-pencil.svg';

export default function FilterHorizontal ({categoryList, categorySelected, onSelectedSchedule}) {

  const renderCategories = ({item, index}) => {
    const itemTitle = result(item, 'title', '');
    const backgroundColor = item.key === categorySelected ? ownColor.primary : 'white';
    const color = item.key === categorySelected ? 'white' : '#B2B7C6';
    return (
      <Box py='1' px='3' mr='3' bg={backgroundColor} borderWidth='1' borderRadius='20' borderColor='#B2B7C6'>
        <Pressable onPress={onSelectedSchedule({item, index})}>
          <Text color={color} textAlign='center'>{itemTitle}</Text>
        </Pressable>
      </Box>
    );
  };

  return (
    <Box>
      <FlatList
        data={categoryList}
        renderItem={renderCategories}
        keyExtractor={(item) => item.key}
        horizontal={true}
      />
    </Box>
  );
}

FilterHorizontal.defaultProps = {
  categoryList: [
    {key: 'all', title: 'All'},
    {key: 'announcement', title: 'Announcement'},
    {key: 'news', title: 'News'},
  ],
  categorySelected: '',
  onSelectedSchedule: () => {},
};

FilterHorizontal.propTypes = {
  categoryList: PropTypes.arrayOf(PropTypes.shape({})),
  categorySelected: PropTypes.string,
  onSelectedSchedule: PropTypes.func
};