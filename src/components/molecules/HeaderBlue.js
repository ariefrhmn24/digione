import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {result} from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import {Box, View, HStack, Text, Pressable} from 'native-base';
import {PropTypes} from 'prop-types';
import {useTranslation} from 'react-i18next';

import IcChevronLeft from 'assets/svg/ic-chevron-left.svg';

function HeaderBlue ({
  title, 
  isBack, 
  isEmpty, 
  onPressHeader
}) {
  const {t, i18n} = useTranslation();
  const constantRdx = useSelector((state) => state.constantRdx);
  const titleRdx = result(constantRdx, 'headerTitle', '');
  const titleHeader = title === '' ? titleRdx : t(title);
  return (
    <View>
      <LinearGradient
        colors={['#0042A6', '#507DF1']}
        // style={styles.linearGradient}
        useAngle={true}
        angle={104}
      >
        <Box safeAreaTop >
          <HStack py='3'>
            { isBack ? 
              <Box flex={1} alignItems='flex-start' alignSelf='center'>
                <Pressable ml='3' onPress={onPressHeader}>
                  <IcChevronLeft width={25} height={25} fill='white' />
                </Pressable>
              </Box> 
              : 
              <View flex={1} /> 
            }
            <View flex={2} alignSelf='center'>
              {isEmpty ?
                <Text textAlign='center' color='white' fontSize='xl'>{''}</Text>
                :
                <Text textAlign='center' color='white' fontSize='xl'>{titleHeader}</Text>
              }
            </View>
            <View flex={1} />
          </HStack>
        </Box>
      </LinearGradient>
    </View>
  );
}

export default HeaderBlue;

HeaderBlue.defaultProps = {
  title: '',
  isBack: true,
  isEmpty: false,
  onPressHeader: () => {},
};

HeaderBlue.propTypes = {
  title: PropTypes.string,
  isBack: PropTypes.bool,
  isEmpty: PropTypes.bool,
  onPressHeader: PropTypes.func,
};