import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';

import IcPencil from 'assets/svg/ic-pencil.svg';

export default function ProfileCard ({title, subTitle, subTitle2, url, rightIcon}) {
  let initials = title.split(' ');
  
  if (initials.length > 1) {
    initials = initials.shift().charAt(0) + initials.pop().charAt(0);
  } else {
    initials = title.substring(0, 2);
  }

  const avatar = () => (
    <Box>
      { url !== '' ?
        <Avatar size='lg' bg='green.500' source={{uri: url}}>{initials}</Avatar>
        :
        <Avatar size='lg' bg='green.500'>{initials}</Avatar>
      }
    </Box>
  );

  return (
    <Box>
      <HStack 
        p='5' shadow='2'
        justifyContent='space-between' 
        bg='white' borderRadius='10'>
        <HStack space='3' alignItems='center'>
          {avatar()}
          <VStack space='1'>
            <Text fontWeight='bold'>{title}</Text>
            <Text>{subTitle}</Text>
            <Text>{subTitle2}</Text>
          </VStack>
        </HStack>
        <Box>
          {rightIcon &&
            <IcPencil width='25' height='25' />
          }
        </Box>
      </HStack>
    </Box>
  );
}

ProfileCard.defaultProps = {
  title: '',
  subTitle: '',
  subTitle2: '',
  url: '',
  rightIcon: true
};

ProfileCard.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  subTitle2: PropTypes.string,
  url: PropTypes.string,
  rightIcon: PropTypes.bool
};