import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {result} from 'lodash';
import {Box, VStack, HStack, Text, Image, AspectRatio} from 'native-base';
import {PropTypes} from 'prop-types';
import {useTranslation} from 'react-i18next';

import TeacherCard from 'components/molecules/TeacherCard';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import ClassFullCard from 'components/molecules/ClassFullCard';

function PopularClasses ({
  title,
  classes,
  author
}) {
  const {t, i18n} = useTranslation();

  return (
    <VStack space={4}>
      <HStack justifyContent='space-between' alignItems='center'>
        <Text fontSize='lg' fontWeight='semibold'>{title}</Text>
        <IcChevronRight />
      </HStack>

      <ClassFullCard
        classes={classes}
        author={author} />

    </VStack>
  );
}

export default PopularClasses;

PopularClasses.defaultProps = {
  title: '',
  isBack: true,
  isEmpty: true,
  onPressHeader: () => {},
};

PopularClasses.propTypes = {
  title: PropTypes.string,
  isBack: PropTypes.bool,
  isEmpty: PropTypes.bool,
  onPressHeader: PropTypes.func,
};