import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, View} from 'native-base';
import {useTranslation} from 'react-i18next';
import _ from 'lodash';

import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import IcPencil from 'assets/svg/ic-pencil.svg';

export default function MenuList ({menuTitle, menuData}) {
  const {t, i18n} = useTranslation();

  const renderItem = (item, index) => (
    <VStack key={index} space='3'>
      <Pressable onPress={item.onPress}>
        <HStack justifyContent='space-between'>
          <Text>{t(item.label)}</Text>
          <HStack space='1'>
            <Text color='text.100'>{item.subLabel}</Text>
            <IcChevronRight width='25' height='25' />
          </HStack>
        </HStack>
      </Pressable>
      { index === (menuData.length - 1) ?
        <Box />
        :
        <Divider width='100%' />
      }
    </VStack>
  );

  return (
    <Box p='5' bg='white'>
      <VStack space='3'>
        <Text mb='3' fontSize='lg' fontWeight='bold'>{t(menuTitle)}</Text>
        {menuData.map((item, i) => renderItem(item, i))}
      </VStack>
    </Box>
  );
}

MenuList.defaultProps = {
  menuTitle: 'Menu Title',
  menuData: [
    {
      label: 'Menu 1',
      onPress: () => {},
    },
    {
      label: 'Menu 2',
      onPress: () => {},
    },
    {
      label: 'Menu 3',
      onPress: () => {},
    }
  ]
};

MenuList.propTypes = {
  menuTitle: PropTypes.string,
  menuData: PropTypes.arrayOf(PropTypes.shape({})),
};