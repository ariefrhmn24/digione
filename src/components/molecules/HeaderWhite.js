import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {result} from 'lodash';
import {Box, View, HStack, Text, Pressable} from 'native-base';
import {PropTypes} from 'prop-types';

import IcChevronLeftFill from 'assets/svg/ic-chevron-left-fill.svg';

function HeaderWhite ({
  title, 
  isBack, 
  onPressHeader
}) {
  const constantRdx = useSelector((state) => state.constantRdx);
  const titleRdx = result(constantRdx, 'headerTitle', '');
  const titleHeader = title === '' ? titleRdx : title;

  return (
    <View>
      <Box safeAreaTop bg='white'>
        <HStack py='3'>
          { isBack ? 
            <Box flex={1} alignItems='flex-start' alignSelf='center'>
              <Pressable ml='4' onPress={onPressHeader}>
                <IcChevronLeftFill width={40} height={40} />
              </Pressable>
            </Box> 
            : 
            <View flex={1} /> 
          }
          <View flex={2} alignSelf='center'>
            <Text textAlign='center' fontSize='lg'>{titleHeader}</Text>
          </View>
          <View flex={1} />
        </HStack>
      </Box>
    </View>
  );
}

export default HeaderWhite;

HeaderWhite.defaultProps = {
  title: '',
  isBack: true,
  isEmpty: true,
  onPressHeader: () => {},
};

HeaderWhite.propTypes = {
  title: PropTypes.string,
  isBack: PropTypes.bool,
  isEmpty: PropTypes.bool,
  onPressHeader: PropTypes.func,
};