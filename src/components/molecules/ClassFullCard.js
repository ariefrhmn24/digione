import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, AspectRatio, Text, HStack, Image, Spacer, Link, Pressable, ScrollView} from 'native-base';

import IcPencil from 'assets/svg/ic-pencil.svg';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';

function ClassFullCard ({
  name, 
  classes, 
  author, 
  url
}) {
  let initials = name.split(' ');
  
  if (initials.length > 1) {
    initials = initials.shift().charAt(0) + initials.pop().charAt(0);
  } else {
    initials = name.substring(0, 2);
  }

  return (
    <VStack shadow='2' bg='white' borderRadius='10'>
      <AspectRatio ratio={3 / 1}>
        <Image 
          w='100%'
          h='100%'
          borderTopRadius='10'
          resizeMode='cover'
          source={require('assets/img/dummy-news-1.png')}
          alt='Classes' />
      </AspectRatio>
       
      <HStack p='2' justifyContent='space-between'>
        <VStack>
          <Text>{classes}</Text>
          <Text color='blueGray.400'>{author}</Text>
        </VStack>
        <Box alignSelf='center'>
          {/* <IcChevronRight width='25' height='25' /> */}
        </Box>
      </HStack>
    </VStack>
  );
}

export default ClassFullCard;

ClassFullCard.defaultProps = {
  name: 'Xpert',
  subject: 'Korean',
  seats: '5',
  url: ''
};

ClassFullCard.propTypes = {
  name: PropTypes.string,
  subject: PropTypes.string,
  seats: PropTypes.string,
  url: PropTypes.string,
};