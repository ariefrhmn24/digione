/* eslint-disable react/jsx-no-bind */
import React, {useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView, Button} from 'native-base';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import useCountDown from 'react-countdown-hook';
import {useTranslation} from 'react-i18next';

export default function Countdown ({
  onResendOtp
}) {
  const {t, i18n} = useTranslation();
  const initialTime = 10 * 1000; // initial time in milliseconds, defaults to 60000
  const interval = 1000;

  const [timeLeft, {start, pause, resume, reset}] = useCountDown(initialTime, interval);
  
  const restart = React.useCallback(() => {
    // you can start existing timer with an arbitrary value
    // if new value is not passed timer will start with initial value
    const newTime = 42 * 1000;
    start(newTime);
  }, []);

  // start the timer during the first render
  useEffect(() => {
    start();
  }, []);

  const tryResendOtp = () => {
    if (timeLeft === 0) {
      start();
      onResendOtp();
    }
  };

  return (
    <Box alignSelf='center' flexDirection='row'>
      <Text>{t('txtEmailVerify3')} </Text>
      <Link onPress={tryResendOtp}>
        <Text 
          color={timeLeft === 0 ? 'brand.500' : 'text.500'}
          underline={timeLeft === 0}
        >{t('txtResend')}</Text>
      </Link>
      <Text> {t('txtIn')} {timeLeft / 1000}s</Text>
    </Box>
  );
}

Countdown.defaultProps = {
  onResendOtp: () => {},
};

Countdown.propTypes = {
  onResendOtp: PropTypes.func,
};

const styles = StyleSheet.create({
  contentCount: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
