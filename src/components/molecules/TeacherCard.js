import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';

import IcPencil from 'assets/svg/ic-pencil.svg';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';

export default function TeacherCard ({
  name, 
  subject, 
  seats, 
  url
}) {
  let initials = name.split(' ');
  
  if (initials.length > 1) {
    initials = initials.shift().charAt(0) + initials.pop().charAt(0);
  } else {
    initials = name.substring(0, 2);
  }

  return (
    <Box>
      <HStack 
        p='5' shadow='2'
        justifyContent='space-between' 
        bg='white' borderRadius='10'>
        <HStack space='5' alignItems='center'>
          <Avatar size='lg' bg='green.500' source={{uri: url}}>{initials}</Avatar>
          <VStack space='1'>
            <Text fontWeight='bold'>{name}</Text>
            <Text>{subject}</Text>
            <Text color='secondary.500'>{seats} seat left!</Text>
          </VStack>
        </HStack>
        <Box alignSelf='center'>
          <IcChevronRight width='25' height='25' />
        </Box>
      </HStack>
    </Box>
  );
}

TeacherCard.defaultProps = {
  name: 'Xpert',
  subject: 'Korean',
  seats: '5',
  url: ''
};

TeacherCard.propTypes = {
  name: PropTypes.string,
  subject: PropTypes.string,
  seats: PropTypes.string,
  url: PropTypes.string,
};