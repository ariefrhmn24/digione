import React from 'react';
import {Button} from 'native-base';
import {StyleSheet} from 'react-native';
import {ownColor} from 'theme/color';
import {ownSize} from 'theme/size';

export const MyButton = ({
  props,
  onPress,
  text,
  isLoading,
  isLoadingText,
  size,
  variant,
}) => {
  const customBold = '';

  return (
    <Button 
      {...this.props}
      onPress={onPress} 
      isLoading={isLoading} 
      isLoadingText={isLoadingText}
      size={size || 'lg'}
      variant={variant || 'solid'} // solid, link, ghost, outline
    >
      {text}
    </Button>
  );
};

const styles = StyleSheet.create({
  textSuperBold: {
    fontSize: ownSize.semiBig,
    color: ownColor.black,
    fontFamily: 'Roboto-Bold',
  },
});
