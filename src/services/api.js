import {
  BASE_URL, 
  TIMEOUT, 
  API_VERSION, 
  SERVICE_AUTH, 
  SERVICE_UTIL, 
  SERVICE_REGIST
} from '../config';
import {mock_apiLogin, mock_apiProfile, mock_apiSubjectList, mock_apiScheduleList, mock_apiSetName} from './mockApi';
import axios from 'axios';
import {result} from 'lodash';
import AsyncStorage from '@react-native-async-storage/async-storage';

const handleResponse = (response, uri) => {
  console.log(uri, response);
  return response;
};

export const headerApiLogin = async () => {
  const token = await AsyncStorage.getItem('accessToken');
  const header = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'token': token,
  };
  return header;
};

export const headerApi = async () => {
  const header = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  };
  return header;
};

// REGISTER
export const apiRegister = async (payload) => {
  const header = await headerApiLogin();
  // const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/${API_VERSION}/register`;
  const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/v2/register`;
  const response = await axios.post(url, payload, {
    timeout: TIMEOUT,
    headers: header
  });
  return handleResponse(response, url);
};

// VERIFY OTP
export const apiVerifyOtp = async (payload) => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/${API_VERSION}/otp/verify`;
  const response = await axios.post(url, payload, {
    timeout: TIMEOUT,
    headers: header
  });
  return handleResponse(response, url);
};

// SEND RESEND OTP
export const apiResendOtp = async (payload) => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/${API_VERSION}/otp/send/email`;
  const response = await axios.post(url, payload, {
    timeout: TIMEOUT,
    headers: header
  });
  return handleResponse(response, url);
};

// SET NAME
export const apiSetName = async (payload) => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/${API_VERSION}/setName`;
  // const response = await axios.post(url, payload, {
  //   timeout: TIMEOUT,
  //   headers: header
  // });
  const response = mock_apiSetName;
  return handleResponse(response, url);
};

// LOGIN
export const apiLogin = async (payload) => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/public/${SERVICE_AUTH}/${API_VERSION}/userSignin`;
  const response = await axios.post(url, payload, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiLogin;
  return handleResponse(response, url);
};

// SET PASSWORD
export const apiSetPassword = async (payload) => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/public/${SERVICE_REGIST}/${API_VERSION}/setPassword`;
  const response = await axios.post(url, payload, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiSetPassword;
  return handleResponse(response, url);
};

// GET USER PROFILE
export const apiProfile = async () => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/restricted/user/${API_VERSION}/profile`;
  const response = await axios.get(url, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiProfile;
  return handleResponse(response, url);
};

// GET SUBJECT LIST
export const apiSubjectList = async () => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/restricted/app/subject/${API_VERSION}/list`;
  const response = await axios.get(url, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiSubjectList;
  return handleResponse(response, url);
};

// GET SCHEDULE LIST
export const apiScheduleList = async () => {
  const header = await headerApiLogin();
  const url = `${BASE_URL}/api/restricted/app/schedule/${API_VERSION}/list`;
  const response = await axios.get(url, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiScheduleList;
  return handleResponse(response, url);
};

// GET TEACHER LIST
export const apiTeacherList = async () => {
  const header = await headerApi();
  const url = `${BASE_URL}/api/public/${SERVICE_UTIL}/${API_VERSION}/tutor/list`;
  const response = await axios.get(url, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiProfile;
  return handleResponse(response, url);
};

// GET TEACHER DETAIL
export const apiTeacherDetail = async (payload) => {
  console.log('payload 3', payload);
  const id = result(payload, 'id', '');
  const header = await headerApi();
  const url = `${BASE_URL}/api/public/${SERVICE_UTIL}/${API_VERSION}/tutor/detail/${id}`;
  const response = await axios.get(url, {
    timeout: TIMEOUT,
    headers: header
  });
  // const response = mock_apiProfile;
  return handleResponse(response, url);
};


// export const apiLoginx = async (payload) => {
//   const uri = `${BASE_URL}/api/public/${SERVICE_AUTH}/${API_VERSION}/userSignin`;
//   const res = await fetch(uri, {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//       Accept: 'application/json',
//     },
//     body: JSON.stringify(payload),
//   });
//   return handleResponse(res, uri);
// };

// export const apiLoginz = async (payload) => {
//   const url = `${BASE_URL}/api/public/${SERVICE_AUTH}/${API_VERSION}/userSignin`;
//   try {
//     const response = await axios.post(url, payload, {
//       timeout: TIMEOUT
//     });
//     return handleResponse(response, url);
//   } catch (error) {
//     return handleResponse(error.response, url);
//   }
// };

// GET
// export const apiLoginv = async (payload) => {
// const url = 'http://www.omdbapi.com?apikey=a329e40b&s=Batman&page=2';
// try {
//   const response = await axios.get(url, {
//     timeout: TIMEOUT
//   });
//   return handleResponse(response, url);
// } catch (error) {
//   return handleResponse(error.response, url);
// }

//   const url = 'http://www.omdbapi.com?apikey=a329e40b&s=Batman&page=2';
//   const response = await axios.get(url, {
//     timeout: TIMEOUT
//   });
//   return handleResponse(response, url);
// };