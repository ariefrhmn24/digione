export const mock_apiLogin = {
  'status': 200,
  'data': {
    'success': true,
    'message': 'success',
    'accessToken': 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2IiwiZW1haWwiOiJhcmllZkBtYWlsLmNvbSIsImlhdCI6MTYzNDg5Nzg4MCwiZXhwIjoxNjM0OTIyMDAwfQ.DcwKphvljRtORdJ2xZp-9nWuV55yh61O8VP94y2314Ci3fJFh3OgG0B9waMbEC6uiU_aGg9xHJFQWw1p-s3zCQ',
    'tokenType': 'Bearer',
    'additionalInfo': {},
    'currentLoginTime': '2021-10-22T17:18:01.036',
    'authToken': null,
    'userPrincipal': {
      'id': 6,
      'name': 'Arief Rahman',
      'nik': null,
      'email': 'arief@mail.com',
      'mobileNumber': null,
      'lastSuccessLogin': '2021-10-20T14:02:04',
      'lastFailLogin': null,
      'lastChangedPassword': null,
      'lastSubmitForm': null,
      'passFail': 0,
      'lastAccessAt': '2021-10-20T14:02:14',
      'authorities': [],
      'userType': {
        'id': 1,
        'createdAt': '2021-08-25T00:15:46',
        'updatedAt': null,
        'createdBy': 'SYSTEM',
        'updatedBy': null,
        'code': 'student',
        'name': 'Student',
        'description': 'Student',
        'type': 'user',
        'hibernateLazyInitializer': {}
      },
      'status': {
        'id': 1,
        'createdAt': '2021-08-25T00:18:22',
        'updatedAt': null,
        'createdBy': 'SYSTEM',
        'updatedBy': null,
        'type': 'user',
        'code': 'active',
        'name': 'User Active',
        'state': null,
        'description': 'User Active',
        'hibernateLazyInitializer': {}
      },
      'username': 'arief@mail.com',
      'enabled': true,
      'fromLoginByUsername': true,
      'accountNonExpired': true,
      'accountNonLocked': true,
      'credentialsNonExpired': true
    }
  }
};

export const mock_apiProfile = {
  'status': 200,
  'data': {
    'success': true,
    'message': 'success',
    'data': {
      'name': 'Arief Rahman',
      'schoolName': '',
      'email': 'arief@mail.com',
      'mobileNumber': null,
      'userName': '1'
    }
  }
};

export const mock_apiSubjectList = {
  'status': 200,
  'data': {
    'success': true,
    'message': 'success',
    'data': [
      {
        'id': 4,
        'code': 'SBJ00000031',
        'name': 'Fisika',
        'description': 'FISIKA 11 IPA'
      },
      {
        'id': 5,
        'code': 'SBJ00000032',
        'name': 'Biologi',
        'description': 'BIOLOGI 11 IPA'
      },
      {
        'id': 15,
        'code': 'SBJ00000042',
        'name': 'Bahasa Indonesia',
        'description': 'BAHASA INDONESIA 11 IPA'
      },
      {
        'id': 16,
        'code': 'SBJ00000043',
        'name': 'Matematika',
        'description': 'MATEMATIKA 11 IPA'
      },
      {
        'id': 17,
        'code': 'SBJ00000044',
        'name': 'Agama',
        'description': 'AGAMA 11 IPA'
      },
      {
        'id': 18,
        'code': 'SBJ00000045',
        'name': 'Bahasa Inggris',
        'description': 'BAHASA INGGRIS 11 IPA'
      }
    ]
  }
};

export const mock_apiScheduleList = {
  'status': 200,
  'data': {
    'success': true,
    'message': 'success',
    'data': [
      {
        'days': 'Senin',
        'time': [
          {
            'subject': {
              'id': 4,
              'code': 'SBJ00000031',
              'name': 'Fisika',
              'description': 'FISIKA 11 IPA'
            },
            'timeStartEnd': [
              '06:00',
              '07:00'
            ]
          },
          {
            'subject': {
              'id': 5,
              'code': 'SBJ00000032',
              'name': 'Biologi',
              'description': 'BIOLOGI 11 IPA'
            },
            'timeStartEnd': [
              '07:00',
              '08:00'
            ]
          }
        ]
      },
      {
        'days': 'Selasa',
        'time': [
          {
            'subject': {
              'id': 15,
              'code': 'SBJ00000042',
              'name': 'Bahasa Indonesia',
              'description': 'BAHASA INDONESIA 11 IPA'
            },
            'timeStartEnd': [
              '07:00',
              '08:00'
            ]
          },
          {
            'subject': {
              'id': 16,
              'code': 'SBJ00000043',
              'name': 'Matematika',
              'description': 'MATEMATIKA 11 IPA'
            },
            'timeStartEnd': []
          }
        ]
      },
      {
        'days': 'Rabu',
        'time': [
          {
            'subject': {
              'id': 17,
              'code': 'SBJ00000044',
              'name': 'Agama',
              'description': 'AGAMA 11 IPA'
            },
            'timeStartEnd': [
              '08:00',
              '10:00'
            ]
          },
          {
            'subject': null,
            'timeStartEnd': []
          }
        ]
      },
      {
        'days': 'Kamis',
        'time': [
          {
            'subject': {
              'id': 18,
              'code': 'SBJ00000045',
              'name': 'Bahasa Inggris',
              'description': 'BAHASA INGGRIS 11 IPA'
            },
            'timeStartEnd': [
              '07:00',
              '09:00'
            ]
          },
          {
            'subject': {
              'id': 4,
              'code': 'SBJ00000031',
              'name': 'Fisika',
              'description': 'FISIKA 11 IPA'
            },
            'timeStartEnd': [
              '10:00',
              '12:00'
            ]
          },
          {
            'subject': {
              'id': 5,
              'code': 'SBJ00000032',
              'name': 'Biologi',
              'description': 'BIOLOGI 11 IPA'
            },
            'timeStartEnd': [
              '13:00',
              '14:00'
            ]
          }
        ]
      },
      {
        'days': 'Jumat',
        'time': [
          {
            'subject': {
              'id': 15,
              'code': 'SBJ00000042',
              'name': 'Bahasa Indonesia',
              'description': 'BAHASA INDONESIA 11 IPA'
            },
            'timeStartEnd': [
              '09:00',
              '11:00'
            ]
          },
          {
            'subject': null,
            'timeStartEnd': []
          },
          {
            'subject': {
              'id': 18,
              'code': 'SBJ00000045',
              'name': 'Bahasa Inggris',
              'description': 'BAHASA INGGRIS 11 IPA'
            },
            'timeStartEnd': [
              '13:00',
              '14:00'
            ]
          }
        ]
      }
    ]
  }
};

export const mock_apiSetName = {
  'status': 200,
  'data': {
    'id': 14, 'createdAt': '2022-02-09T11:05:39', 'updatedAt': '2022-02-09T11:06:49.93', 'createdBy': 'Ariefrhmn.24@gmail.com', 'updatedBy': 'SYSTEM', 'name': null, 'email': 'Ariefrhmn.24@gmail.com', 'mobileNumber': null, 'referenceNumber': 'REG00000014', 'isVerifyEmail': true, 'isVerifyMobileNumber': false, 'countSendOtpEmail': 1, 'countSendOtpMobileNumber': 0, 'countVerifyOtpEmail': 1, 'countVerifyOtpMobileNumber': 0, 'lastStep': 'REGISTER_COMPLETE', 'deviceId': null
  }
};