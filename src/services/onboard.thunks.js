import AsyncStorage from '@react-native-async-storage/async-storage';
import {result} from 'lodash';
import {Alert} from 'react-native';

import * as RootNavigation from 'routes/navigationService';
import {apiLogin, apiSetPassword} from 'services/api';
import {saveLogin} from 'redux/loginReducer';
import {saveAccessToken} from 'redux/constantReducer';

export const postLogin = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();

  apiLogin(payload).then(async (res) => {
    // Alert.alert(JSON.stringify(res));
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const accessToken = result(resData, 'accessToken', '');
        await AsyncStorage.setItem('accessToken', accessToken);  
        dispatch(saveAccessToken(accessToken));
        RootNavigation.navigate('ExplorePage');
        // const userPrincipal = result(resData, 'userPrincipal', {});
        // const name = result(userPrincipal, 'name', '');
        // const email = result(userPrincipal, 'email', '');
        // const mobileNumber = result(userPrincipal, 'mobileNumber', '');
        // const objUser = {
        //   accessToken, name, email, mobileNumber
        // };
        // await AsyncStorage.setItem('user', JSON.stringify(objUser));  
        // dispatch(saveLogin(resData));
        // RootNavigation.navigate('PasswordPage');
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    // Alert.alert(JSON.stringify(err));
    callback(err);
  }).finally(() => {
    final(false);
  });

  // RootNavigation.navigate('PasswordPage');
};

export const postSetPassword = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();

  // apiSetPassword(payload).then(async (res) => {
  //   if (res.status === 200) {
  //     const resData = result(res, 'data', {});
  //     if (resData.success) {
  //       RootNavigation.navigate('ExplorePage');
  //     } else {
  //       callback(res);
  //     }
  //   }
  // }).catch((err) => {
  //   callback(err);
  // }).finally(() => {
  //   final(false);
  // });

  RootNavigation.navigate('ExplorePage');
  // RootNavigation.reset({
  //   index: 0,
  //   routes: [{name: 'HomeScreen'}],
  // });
};