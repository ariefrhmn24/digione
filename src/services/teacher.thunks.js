import AsyncStorage from '@react-native-async-storage/async-storage';
import {result} from 'lodash';
import {Alert} from 'react-native';

import * as RootNavigation from 'routes/navigationService';
import {
  apiTeacherList,
  apiTeacherDetail
} from 'services/api';
import {
  saveTeacherList,
  saveTeacherDetail
} from 'redux/teacherReducer';

export const getTeacherList = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  apiTeacherList(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const data = result(resData, 'data', {});
        dispatch(saveTeacherList(data));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const getTeacherDetail = (payload, callback, final) => async (dispatch, getState) => {
  console.log('payload 2', payload);
  const currentState = getState();
  apiTeacherDetail(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const data = result(resData, 'data', {});
        dispatch(saveTeacherDetail(data));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};


