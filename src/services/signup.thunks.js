import AsyncStorage from '@react-native-async-storage/async-storage';
import {result} from 'lodash';
import {Alert} from 'react-native';

import * as RootNavigation from 'routes/navigationService';
import {
  apiRegister, 
  apiVerifyOtp, 
  apiResendOtp, 
  apiSetPassword,
  apiSetName
} from 'services/api';
import {
  savePostRegister,
  savePostVerify
} from 'redux/signupReducer';
import {
  saveLogin
} from 'redux/loginReducer';
export const postRegister = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const email = signupRdx.email;
  
  const payload = {
    email
  };
  apiRegister(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const saveData = result(resData, 'data', {});
        dispatch(savePostRegister(saveData));

        RootNavigation.navigate('SignupOtpPage');
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const postVerifyOtp = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const referenceNumber = signupRdx.postRegister.referenceNumber;
  const type = signupRdx.type;
  
  const payload = {
    ...params,
    referenceNumber,
    type,
  };
  apiVerifyOtp(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const saveData = result(resData, 'data', {});
        // dispatch(savePostVerify(saveData));

        RootNavigation.navigate('PasswordPage');
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    // console.log('err', JSON.parse(JSON.stringify(err)));
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const postResendOtp = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const referenceNumber = signupRdx.postRegister.referenceNumber;
  
  const payload = {
    referenceNumber,
  };
  apiResendOtp(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      console.log('resData', resData);
      if (resData.success) {
        const saveData = result(resData, 'data', {});
        dispatch(savePostRegister(saveData));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const postSetPassword = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const referenceNumber = signupRdx.postRegister.referenceNumber;
  
  const payload = {
    ...params,
    referenceNumber,
  };
  apiSetPassword(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        // const saveData = result(resData, 'data', {});
        // dispatch(savePostRegister(saveData));
        RootNavigation.navigate('SignupIdPage');
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
    // RootNavigation.navigate('SignupIdPage');
  }).finally(() => {
    final(false);
  });
};

export const postSetName = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const referenceNumber = signupRdx.postRegister.referenceNumber;
  
  const payload = {
    ...params,
    referenceNumber,
  };
  apiSetName(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const saveData = result(resData, 'data', {});
        dispatch(saveLogin(saveData));
        RootNavigation.navigate('SignupCompletedPage');
      } else {
        dispatch(saveLogin(resData));
        RootNavigation.navigate('SignupCompletedPage');
        // callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
    // RootNavigation.navigate('SignupCompletedPage');
  }).finally(() => {
    final(false);
  });
};
