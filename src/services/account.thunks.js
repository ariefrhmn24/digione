import AsyncStorage from '@react-native-async-storage/async-storage';
import {result} from 'lodash';
import {Alert} from 'react-native';

import * as RootNavigation from 'routes/navigationService';
import {apiProfile} from 'services/api';
import {
  saveProfile,
  resetAccountRdx
} from 'redux/accountReducer';
import {resetSignupRdx} from 'redux/signupReducer';
import {resetLoginRdx} from 'redux/loginReducer';
import {resetConstantRdx} from 'redux/constantReducer';
import {resetScheduleRdx} from 'redux/scheduleReducer';
import {resetSubjectRdx} from 'redux/subjectReducer';
import {resetTeacherRdx} from 'redux/teacherReducer';

export const getProfile = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();

  apiProfile(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const profile = result(resData, 'data', {});
        dispatch(saveProfile(profile));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const postLogout = (params, callback, final) => async (dispatch, getState) => {
  const currentState = getState();
  const signupRdx = currentState.signupRdx;
  const referenceNumber = signupRdx.postRegister.referenceNumber;
  
  dispatch(resetAccountRdx());
  dispatch(resetConstantRdx());
  dispatch(resetLoginRdx());
  dispatch(resetScheduleRdx());
  dispatch(resetSignupRdx());
  dispatch(resetSubjectRdx());
  dispatch(resetTeacherRdx());
  // const payload = {
  //   ...params,
  //   referenceNumber,
  // };
  // apiSetName(payload).then(async (res) => {
  //   if (res.status === 200) {
  //     const resData = result(res, 'data', {});
  //     if (resData.success) {
  //       const saveData = result(resData, 'data', {});
  //       dispatch(saveLogin(saveData));
  //       RootNavigation.navigate('SignupCompletedPage');
  //     } else {
  //       dispatch(saveLogin(resData));
  //       RootNavigation.navigate('SignupCompletedPage');
  //       // callback(res);
  //     }
  //   }
  // }).catch((err) => {
  //   callback(err);
  //   // RootNavigation.navigate('SignupCompletedPage');
  // }).finally(() => {
  //   final(false);
  // });
};
