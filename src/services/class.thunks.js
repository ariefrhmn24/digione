import AsyncStorage from '@react-native-async-storage/async-storage';
import {result} from 'lodash';

import * as RootNavigation from 'routes/navigationService';
import {apiSubjectList, apiScheduleList} from 'services/api';
import {saveSubjectList} from 'redux/subjectReducer';
import {saveScheduleList} from 'redux/scheduleReducer';

export const getSubjectList = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();

  apiSubjectList(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const subjectList = result(resData, 'data', {});
        dispatch(saveSubjectList(subjectList));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};

export const getScheduleList = (payload, callback, final) => async (dispatch, getState) => {
  const currentState = getState();

  apiScheduleList(payload).then(async (res) => {
    if (res.status === 200) {
      const resData = result(res, 'data', {});
      if (resData.success) {
        const scheduleList = result(resData, 'data', {});
        dispatch(saveScheduleList(scheduleList));
      } else {
        callback(res);
      }
    }
  }).catch((err) => {
    callback(err);
  }).finally(() => {
    final(false);
  });
};