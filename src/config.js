import {ENV} from '@env';
import {LogBox} from 'react-native';
import Config from 'react-native-config';
// import jailMonkey from 'jail-monkey';

const PROTOCOL = 'https:';
const PROTOCOL2 = 'http:';
export const API_VERSION = 'v1';

export const SERVICE_AUTH = 'auth';
export const SERVICE_UTIL = 'util';
export const SERVICE_REGIST = 'registration';


// ==== BASE URL ====
export const BASE_URL = `${PROTOCOL2}//${Config.API_URL}`;
// export const BASE_URL = `${Config.API_URL}`;
// export const BASE_URL = 'http://ec2-18-217-200-215.us-east-2.compute.amazonaws.com';

// =============================================
export const TIMEOUT = 600000;
export const MAIL_FORMAT = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
export const FULLNAME_FORMAT = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

export const PLAY_STORE = 'http://play.google.com/store/apps/details?id=';
export const APP_STORE = 'https://apps.apple.com/id/app';

// ===========================================
// CONFIG FOR MAKING NETWORK REQUEST SHOW UP ON DEBUGGER
// ===========================================
GLOBAL.XMLHttpRequest =
    GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
GLOBAL.FormData = GLOBAL.originalFormData || GLOBAL.FormData;
window.__REDUX_DEVTOOLS_EXTENSION__;
window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

LogBox.ignoreAllLogs(true);
