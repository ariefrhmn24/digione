/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {Box, VStack, ScrollView, Avatar, Divider, FlatList, Text, HStack, Image, Center, View, Spacer} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {StyleSheet, RefreshControl, Pressable, Platform} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {isEmpty, isNil, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import SwipeCards from 'react-native-swipe-cards-deck';

import Organism from 'containers/organism/Explore/ExplorePage';
import {getTeacherList} from 'services/teacher.thunks';
import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function ExplorePage() {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const isIos = Platform.OS === 'ios';
  const loginRdx = useSelector((state) => state.loginRdx);
  const constantRdx = useSelector((state) => state.constantRdx);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [myDate, setMyDate] = useState(new Date());
  let [greetings, setGreetings] = useState(t('txtMorning'));
  const [rdxIsLogin, setRdxIsLogin] = useState(result(loginRdx, 'isLogin', false));

  useEffect(() => {
    async function initialize() {
      await onWalktrough();
      await onGreetings();
    }
    initialize();
  }, []);

  useEffect(() => {
    const isLogin = result(loginRdx, 'isLogin', false);
    setRdxIsLogin(isLogin);
  }, [loginRdx]);

  const onWalktrough = async () => {
    const isWalktrough = constantRdx.isWalktrough;
    if (isWalktrough) {
      navigation.navigate('WalktroughPage');
    }
  };

  const onGreetings = () => {
    const hrs = myDate.getHours();
    if (hrs < 12)
      setGreetings(t('txtMorning'));
    else if (hrs >= 12 && hrs <= 18)
      setGreetings(t('txtAfternoon'));
    else if (hrs >= 18 && hrs <= 24)
      setGreetings(t('txtEvening'));
  };

  const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => {
      setRefreshing(false);
    });
  }, []);

  const goToTutors = () => {
    navigation.navigate('TeacherListPage');
  };

  const goToSignup = () => {
    // navigation.navigate('SignupEmailPage');
    // navigation.navigate('PasswordPage');
    // navigation.navigate('SignupIdPage');
    navigation.navigate('SignupCompletedPage');
  };

  return (
    <SafeAreaView header='blue'>
      <Organism 
        isIos={isIos}
        refreshing={refreshing}
        onRefresh={onRefresh}

        greetings={greetings}
        rdxIsLogin={rdxIsLogin}

        goToTutors={goToTutors}
        goToSignup={goToSignup}
      />
    </SafeAreaView>
  );
}

export default ExplorePage;