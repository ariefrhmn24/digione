/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback} from 'react';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, View, Pressable, ScrollView, Image} from 'native-base';
import {StyleSheet, RefreshControl} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {isEmpty, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';

import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {getProfile} from 'services/account.thunks';
import {myToast} from 'components/Toast';
import FilterHorizontal from 'components/molecules/FilterHorizontal';
import UpcomingCard from 'components/molecules/UpcomingCard';
import TaskCard from 'components/molecules/TaskCard';

import IcPencil from 'assets/svg/ic-pencil.svg';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function SubjectDetails() {
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const toast = myToast();
  const accountRdx = useSelector((state) => state.accountRdx);
  const constantRdx = useSelector((state) => state.constantRdx);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [categoryList] = useState([
    {key: 'assignments', title: 'Assignments'},
    {key: 'exams', title: 'Exams'},
  ]);
  const [categorySelected, setCategorySelected] = useState('assignments');

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    // fetchProfile();
    wait(1000).then(() => {
      setRefreshing(false);
    });
  }, []);

  useEffect(() => {
    async function initialize() {
      // await fetchProfile();
    }
    initialize();
  }, []);

  const fetchProfile = () => {
    setIsLoading(true);
    const payload = {};
    dispatch(getProfile(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const onSelectedSchedule = ({item, index}) => () => {
    setCategorySelected(item.key);
  };
 
  return (
    <SafeAreaView header='blue'>
      <VStack flex='1' bg='bg.500'>
        <VStack pt='5' bg='white' flex='1'>
          <Text mx='5' fontWeight='bold' fontSize='lg'>{t('txtUpSchedule')}</Text>
          <UpcomingCard />
        </VStack>
        
        <Divider width='100%' p='2' bg='bg.500' />
        
        <VStack bg='white' flex='2' space='5' p='5'>
          <Text fontWeight='bold' fontSize='lg'>{t('txtToDoList')}</Text>
          <Box>
            <FilterHorizontal 
              categoryList={categoryList} 
              categorySelected={categorySelected} 
              onSelectedSchedule={onSelectedSchedule} />
          </Box>
          <Box >
            <TaskCard />
          </Box>
        </VStack>
      </VStack>
    </SafeAreaView>
  );
}

export default SubjectDetails;

SubjectDetails.defaultProps = {
  name: '',
};

SubjectDetails.propTypes = {
  name: PropTypes.string,
};

const styles = StyleSheet.create({
  linearGradient: {
    height: 80,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    position: 'absolute',
    top: 0,
    width: '100%'
  },
});