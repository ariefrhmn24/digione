/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useRef} from 'react';
import {Box, Skeleton, VStack, Button, FormControl, KeyboardAvoidingView, Text, View} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {StyleSheet, useWindowDimensions} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import OverviewTop from 'containers/scenes/TopTabs/OverviewTop';
import SubjectsTop from 'containers/scenes/TopTabs/SubjectsTop';
import AssignmentsTop from 'containers/scenes/TopTabs/AssignmentsTop';
import ScheduleTop from 'containers/scenes/TopTabs/ScheduleTop';
import AttendanceTop from 'containers/scenes/TopTabs/AttendanceTop';
import InfoTop from 'containers/scenes/TopTabs/InfoTop';
import ForumTop from 'containers/scenes/TopTabs/ForumTop';

import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import {ownColor} from 'theme/color';

function LazyPlaceholder ({route}) {
  return (
    <Box p='5'>
      <VStack space='5'>
        <Skeleton width='200' height={8} />
        <Skeleton height='70px' />
        <Skeleton height='70px' variant='circle' />
      </VStack>
    </Box>
  ); 
}

function ClassPage() {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const constantRdx = useSelector((state) => state.constantRdx);
  
  useEffect(() => {
    
  });

  const layout = useWindowDimensions();
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'overview', title: 'Overview'},
    {key: 'subjects', title: 'Subjects'},
    {key: 'assignments', title: 'Assignments'},
    {key: 'schedule', title: 'Schedule'},
    // {key: 'attendance', title: 'Attendance'},
    {key: 'info', title: 'Info'},
    {key: 'forum', title: 'Forum'},
    
  ]);
  const renderScene = SceneMap({
    overview: OverviewTop,
    subjects: SubjectsTop,
    assignments: AssignmentsTop,
    schedule: ScheduleTop,
    // attendance: AttendanceTop,
    info: InfoTop,
    forum: ForumTop,
  });
  const renderLazyPlaceholder = ({route}) => (<LazyPlaceholder  route={route} />);

  const renderLabel = ({route, focused}) => {
    if (focused) {
      return <Text style={{color: ownColor.primary, width: 'auto', textAlign: 'center'}}> {route.title} </Text>; 
    } return <Text style={{color: '#B2B7C6', width: 'auto', textAlign: 'center'}}> {route.title} </Text>; 
  };
  
  const renderTabBar = (props) => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      labelStyle={styles.label}
      tabStyle={styles.tabStyle}
      renderLabel={renderLabel}
    />
  );

  // const renderLazyPlaceholder = useRef();

  return (
    <Box flex={1} bg='white'>
      <TabView
        lazy
        navigationState={{index, routes}}
        renderScene={renderScene}
        // renderLazyPlaceholder={renderLazyPlaceholder}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={renderTabBar}
        // swipeEnabled={false}
      />
    </Box>
  );
}

export default ClassPage;

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
  linearGradient: {
    height: 150,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    // position: 'absolute',
    // top: 0,
    // width: '100%'
  },
  tabbar: {
    backgroundColor: 'white',
    shadowOpacity: 0,
    elevation: 0,
    shadowColor: 'transparent',
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
      width: 0
    },
  },
  indicator: {
    backgroundColor: ownColor.primary
  },
  label: {
    fontWeight: 'bold',
    color: ownColor.primary,
  },
  tabStyle: {
    width: 'auto',
    paddingHorizontal: 10,
    paddingVertical: 15,
  }
});