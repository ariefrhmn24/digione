/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useEffect, useState} from 'react';
import {Container, View, Link, Box, VStack, Input, Button, FormControl, Text, Center, Spacer, Heading, Image, KeyboardAvoidingView, Divider} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {debounce, result} from 'lodash';
import {useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';
import {Platform, Alert} from 'react-native';

import {doEncrypt} from 'helpers';
import {postLogin} from 'services/onboard.thunks';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import IcChevronLeft from 'assets/svg/ic-chevron-left.svg';
import {myToast} from 'components/Toast';

function FormLogin({onLogin, onForgotPassword, onForgotUser, isLoading}) {
  const {t, i18n} = useTranslation();
  const {control, handleSubmit, errors} = useForm();

  const [show, setShow] = React.useState(false);
  const handleShow = () => setShow(!show);

  const onPressLogin = (data) => {
    onLogin(data);
  };

  const onPressForgotUser = () => {
    onForgotUser();
  };
  
  const onPressForgotPassword = () => {
    onForgotPassword();
  };

  return (
    <Box>
      <VStack width='100%' space={4}>
        <FormControl isInvalid={'userId' in errors}>
          {/* <FormControl.Label>User ID</FormControl.Label> */}
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                onBlur={onBlur }
                placeholder={t('txtUsername')}
                onChangeText={(val) => onChange(val)}
                value={value}
              />
            )}
            name='userId'
            rules={{required: t('txtUserReq'), minLength: 1}}
            defaultValue=''
          />
          <FormControl.ErrorMessage>
            {errors.userId?.message}
          </FormControl.ErrorMessage>
        </FormControl>
        <FormControl isInvalid={'password' in errors}>
          {/* <FormControl.Label>Password</FormControl.Label> */}
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                type={show ? 'text' : 'password'}
                onBlur={onBlur}
                placeholder={t('txtPassword')}
                onChangeText={(val) => onChange(val)}
                value={value}
                InputRightElement={
                  <Button bg='none' shadow='none' variant='unstyled' _text={{color: 'grey'}} onPress={handleShow}>
                    {show ? <IcEyeOpen /> : <IcEyeOpen />}
                  </Button>
                }
              />
            )}
            name='password'
            rules={{required: t('txtPassReq')}}
            defaultValue=''
          />
          <FormControl.ErrorMessage>
            {errors.password?.message}
          </FormControl.ErrorMessage>
        </FormControl>
        <Button 
          onPress={handleSubmit(onPressLogin)} 
          isLoading={isLoading} 
          isLoadingText='Submitting'
          bg='brand.500' 
          py={4}
          borderRadius={10}
        >
          {t('txtLogin')}
        </Button>
        <Box alignSelf='center' flexDirection='row'>
          <Text>{t('txtNoAccount')}</Text>
          <Link href='https://nativebase.io'>
            <Text color='brand.500' underline>{t('txtRegSchool')}</Text>
          </Link>
        </Box>
      </VStack>
    </Box>
  );
}

FormLogin.propTypes = {
  onLogin: PropTypes.func,
  onForgotPassword: PropTypes.func,
  onForgotUser: PropTypes.func,
  isLoading: PropTypes.bool
};

function LoginPage() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const [isLoading, setIsLoading] = useState(false);
  const [word, setWord] = useState('');

  const onLogin = (data) => {
    setIsLoading(true);

    const payload = {
      userId: data.userId,
      password: doEncrypt(data.password),
      type: 'user'
    };

    // navigation.navigate('PasswordPage');

    dispatch(postLogin(payload, 
      (callback) => {
        setWord(JSON.stringify(callback));
        Alert.alert(JSON.stringify(callback));
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const onForgotUser = () => {
    navigation.navigate('ForgotUserIDScreen');
  };

  const onForgotPassword = () => {
    navigation.navigate('ForgotPasswordScreen');
  };
  
  return (
    <Box safeAreaBottom p='5' justifyContent='center' flex={1} bg='white'>
      <Box py={3} />
      <Box>
        <Box width={150} height={150} bg='secondary.500' alignSelf='center'/>
        <Heading alignSelf='center' bold color='brand.500' fontSize='6xl'>skool.</Heading>
      </Box>
      <Box py={3} />
      <Text>{word}</Text>
      <FormLogin
        onLogin={debounce((v) => onLogin(v), 100, {
          trailing: false,
          leading: true,
        })}
        onForgotPassword={debounce(() => onForgotPassword(), 100, {
          trailing: false,
          leading: true,
        })}
        onForgotUser={debounce(() => onForgotUser(), 100, {
          trailing: false,
          leading: true,
        })}
        isLoading={isLoading}
      />
      <Box flex={1} alignContent='flex-end' justifyContent='center' flexDirection='row' flexWrap='wrap'>
        <Text fontSize='sm'>{t('txtAgree1')}</Text>
        <Link>
          <Text fontSize='sm' color='brand.500' underline>{t('txtAgree2')}</Text>
        </Link>{t('txtAgree3')}
        <Link>
          <Text fontSize='sm' color='brand.500' underline>{t('txtAgree4')}</Text>
        </Link> 
      </Box>
      
    </Box>
  );
}

export default LoginPage;
