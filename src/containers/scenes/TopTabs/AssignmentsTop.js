import React, {useState, useEffect} from 'react';
import {Box, VStack, HStack, Avatar, Pressable, KeyboardAvoidingView, Text, View} from 'native-base';
import {StyleSheet} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {result} from 'lodash';

// import {postLogin} from 'services/class.thunks';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import TaskCard from 'components/molecules/TaskCard';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';

function AssignmentsTop({route}) {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const constantRdx = useSelector((state) => state.constantRdx);
  const [update, setUpdate] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  
  // const fetchSubject = async () => {
  //   setIsLoading(true);

  //   const payload = {};
  //   dispatch(postLogin(payload, 
  //     (callback) => {
  //       const responseMsg = result(callback, 'response.data.message', 'Please try again');
  //       toast(responseMsg);
  //     },
  //     (final) => setIsLoading(final)
  //   ));
  // };

  useEffect(() => {
    async function initialize() {
      // await fetchClassList();
    }
    initialize();
  }, [update]);

  return (
    <Box flex={1} bg='white' p='5'>
      <VStack space='5'>
        <Text fontWeight='bold' size='lg'>Your Assignment(s)</Text>
        <TaskCard />
      </VStack>
    </Box>
  );
}

export default AssignmentsTop;

AssignmentsTop.defaultProps = {
  route: {},
};

AssignmentsTop.propTypes = {
  route: PropTypes.shape({}),
};

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
});