/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, HStack, FlatList, Pressable, KeyboardAvoidingView, Text, View, Skeleton, Center} from 'native-base';
import {StyleSheet, useWindowDimensions} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {result, isEmpty} from 'lodash';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import {saveSubjectSelected} from 'redux/subjectReducer';
import {saveHeaderTitle} from 'redux/constantReducer';
import {getScheduleList} from 'services/class.thunks';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import {ownColor} from 'theme/color';

const Friday = ({route}) => (
  <Box m='2' bg='orange.100'>
    <Text>im {route.title}</Text>
  </Box>
);

const LazyPlaceholder = ({route}) => (
  <Box p='5'>
    <VStack space='5'>
      <Skeleton width='200' height={8} />
      <Skeleton height='70px' />
      <Skeleton height='70px' variant='circle' />
    </VStack>
  </Box>
);

function ScheduleTop({route}) {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const scheduleRdx = useSelector((state) => state.scheduleRdx);
  const [update, setUpdate] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [scheduleList, setScheduleList] = useState([]);
  const [dayList] = useState([
    {key: 'mon', title: 'Mon'},
    {key: 'tue', title: 'Tue'},
    {key: 'wed', title: 'Wed'},
    {key: 'thu', title: 'Thu'},
    {key: 'fri', title: 'Fri'},
  ]);
  const [daySelected, setDaySelected] = useState('mon');
  const [dayIndex, setDayIndex] = useState(0);
  const scheduleFlatList = result(scheduleList[dayIndex], 'time', []);

  useEffect(() => {
    if (!isEmpty(scheduleRdx.scheduleList)) {
      const scheduleListrdx = result(scheduleRdx, 'scheduleList', []);
      setScheduleList(scheduleListrdx);
    }
  }, [scheduleRdx]);

  useEffect(() => {
    async function initialize() {
      await fetchSchedule();
    }
    initialize();
  }, []);

  const fetchSchedule = async () => {
    setIsLoading(true);
    const payload = {};
    dispatch(getScheduleList(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const renderLabel = ({route, focused}) => {
    if (focused) {
      return (
        <Box width='60' py='2' bg='primary.500' borderRadius='20'>
          <Text style={{color: 'white', textAlign: 'center'}}> {route.title} </Text>
        </Box>); 
    } 
    return (
      <Box width='60' py='2' borderWidth='2' borderColor='bg.500' borderRadius='20'>
        <Text style={{color: '#B2B7C6', textAlign: 'center'}}> {route.title} </Text>
      </Box>
    ); 
  };

  const renderTabBar = (props) => (
    <Box justifyContent='space-between'>
      <TabBar
        {...props}
        indicatorStyle={styles.indicator}
        style={styles.tabbar}
        labelStyle={styles.label}
        tabStyle={styles.tabStyle}
        renderLabel={renderLabel}
      />
    </Box>
  );

  const onSelectedSchedule = ({item, index}) => () => {
    setDaySelected(item.key);
    setDayIndex(index);
  };

  const renderDay = ({item, index}) => {
    const itemDay = result(item, 'title', '');
    const backgroundColor = item.key === daySelected ? ownColor.primary : 'white';
    const color = item.key === daySelected ? 'white' : '#B2B7C6';
    return (
      <Box py='2' mx='4' width='50px' bg={backgroundColor} borderWidth='1' borderRadius='20' borderColor='#B2B7C6'>
        <Pressable onPress={onSelectedSchedule({item, index})}>
          <Text color={color} textAlign='center'>{itemDay}</Text>
        </Pressable>
      </Box>
    );
  };

  const goToSubjectDetails = ({item, index}) => () => {
    dispatch(saveSubjectSelected(item.subject));
    dispatch(saveHeaderTitle(item.subject.name));
    navigation.navigate('SubjectDetailsPage');
  };

  const renderSchedule = ({item, index}) => {
    const subjectName = result(item, 'subject.name', '');
    const subjectStart = result(item, 'timeStartEnd.0', '');
    const subjectEnd = result(item, 'timeStartEnd.1', '');
    return (
      <VStack mb='4' pt='1' mx='5'>
        <Pressable onPress={goToSubjectDetails({item, index})}>
          <HStack 
            px='3' py='4' shadow='2'
            justifyContent='space-between' 
            borderRadius='10' bg='white' 
            alignItems='center'
          >
            <Text fontWeight='bold' fontSize='md'>{subjectName}</Text>
            <HStack space='2'>
              <Text>{subjectStart} - {subjectEnd}</Text>
              <IcChevronRight width='25' height='25' />
            </HStack>
          </HStack>
        </Pressable>
      </VStack>
    );
  };

  const emptyItem = () => (
    <Box mb='5' mx='5'>
      <VStack space='5'>
        <Skeleton height='55px' borderRadius={10} />
        <Skeleton height='55px' borderRadius={10} />
      </VStack>
    </Box>
  );

  return (
    <Box flex={1} bg='white'>
      {/* <Box px='5' pt='5'>
        <Text fontWeight='bold' fontSize='lg'>Weekly Schedule</Text>
      </Box> */}
      <Box m='5'>
        <Text fontWeight='bold' fontSize='lg'>Weekly Schedule</Text>
      </Box>
      <Center >
        <FlatList
          data={dayList}
          renderItem={renderDay}
          keyExtractor={(item) => item.key}
          horizontal={true}
        />
      </Center>
      <VStack space='5' mt='4' flex='1'>
        <FlatList
          data={scheduleFlatList}
          renderItem={renderSchedule}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={emptyItem}
        />
      </VStack>
      {/* <Box pt='2' flex={1}>
        <TabView
          lazy
          navigationState={{index, routes}}
          renderScene={renderScene}
          // renderLazyPlaceholder={renderLazyPlaceholder}
          onIndexChange={setIndex}
          initialLayout={{width: layout.width}}
          renderTabBar={renderTabBar}
          swipeEnabled={false}
          transitionStyle='scroll'
        />
      </Box> */}
    </Box>
  );
}

export default ScheduleTop;

ScheduleTop.defaultProps = {
  route: {},
};

ScheduleTop.propTypes = {
  route: PropTypes.shape({}),
};

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
  tabbar: {
    marginHorizontal: 20,
    // alignItems: 'flex-start',
    backgroundColor: 'white',
    shadowOpacity: 0,
    elevation: 0,
    shadowColor: 'transparent',
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
      width: 0
    },
  },
  indicator: {
    backgroundColor: 'transparent'
  },
  label: {
    fontWeight: 'bold',
    color: ownColor.primary,
  },
  tabStyle: {
    // width: '100%',
    // paddingLeft: 0,
    // paddingRight: 0,
    // marginLeft: 0,
    // marginRight: 0,
    // marginHorizontal: 8,
    // borderWidth: 1,
    // borderRadius: 50,
    // marginHorizontal: 5,
  }
});