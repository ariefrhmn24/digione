import React, {useState, useEffect} from 'react';
import {Box, Stack, Skeleton, Button, FormControl, KeyboardAvoidingView, Text, View, FlatList} from 'native-base';
import {StyleSheet, TouchableHighlight, Platform} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';

function OverviewTop({route}) {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const constantRdx = useSelector((state) => state.constantRdx);
  
  return (
    <Box flex='1' bg='white' p='5'>
      {/* <Text>im {route.title}…</Text> */}
      {/* <VStack space='5'>
        <Skeleton width='200' height={8} />
        <Skeleton height='70px' />
        <Skeleton height='70px' variant='circle' />
      </VStack> */}
      {/* <Skeleton my={2} height={6}  size={20} /> */}
    </Box>
  );
}

export default OverviewTop;

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
});