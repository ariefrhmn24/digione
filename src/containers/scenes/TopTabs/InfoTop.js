import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, KeyboardAvoidingView, Text, View} from 'native-base';
import {StyleSheet} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';

function InfoTop({route}) {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const constantRdx = useSelector((state) => state.constantRdx);
  
  return (
    <Box flex={1} bg='white'>
      <Text>im {route.title}…</Text>
    </Box>
  );
}

export default InfoTop;

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
});