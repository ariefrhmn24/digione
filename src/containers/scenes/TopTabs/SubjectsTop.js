import React, {useState, useEffect, useCallback} from 'react';
import {Box, VStack, HStack, Avatar, Pressable, FlatList, Text, View, Skeleton, ScrollView} from 'native-base';
import {StyleSheet, RefreshControl} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {result, isEmpty, debounce} from 'lodash';

import {getSubjectList} from 'services/class.thunks';
import {saveSubjectSelected} from 'redux/subjectReducer';
import {saveHeaderTitle} from 'redux/constantReducer';
import {myToast} from 'components/Toast';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function SubjectsTop({route}) {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const subjectRdx = useSelector((state) => state.subjectRdx);
  const [update, setUpdate] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [subjectList, setSubjectList] = useState([]);

  useEffect(() => {
    if (!isEmpty(subjectRdx.subjectList)) {
      const subjectListrdx = result(subjectRdx, 'subjectList', []);
      setSubjectList(subjectListrdx);
    }
  }, [subjectRdx]);

  useEffect(() => {
    async function initialize() {
      await fetchSubject();
    }
    initialize();
  }, []);

  const fetchSubject = async () => {
    setIsLoading(true);
    const payload = {};
    dispatch(getSubjectList(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const goToSubjectDetails = ({item, index}) => () => {
    dispatch(saveSubjectSelected(item));
    dispatch(saveHeaderTitle(item.name));
    navigation.navigate('SubjectDetailsPage');
  };

  const renderItem = ({item, index}) => {
    const borderBottom = index !== subjectList.length - 1;
    const subjectName = result(item, 'name', '');
    return (
      <VStack mb='5' mx='5'>
        <Pressable onPress={goToSubjectDetails({item, index})}>
          <HStack 
            p='3' shadow='2'
            justifyContent='space-between' 
            borderRadius='10' bg='white' 
            alignItems='center'
          >
            <HStack alignItems='center' space='3'>
              <Avatar size='md' bg='orange.500' />
              <Text fontWeight='bold' fontSize='md'>{subjectName}</Text>
            </HStack>
            <IcChevronRight width='25' height='25' />
          </HStack>
        </Pressable>
      </VStack>
    );
  };

  const emptyItem = () => (
    <Box mb='5' mx='5'>
      <VStack space='5'>
        <Skeleton height='70px' borderRadius={10} />
        <Skeleton height='70px' borderRadius={10} />
      </VStack>
    </Box>
  );

  return (
    <Box flex='1' bg='white'>
      {/* <ScrollView showsHorizontalScrollIndicator={false}> */}
      <VStack mt='5' flex='1'>
        <Text mx='5' mb='4' fontWeight='bold' fontSize='lg'>{t('txtYourSubject')}</Text>
        <FlatList
          pt='1'
          data={subjectList}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          ListEmptyComponent={emptyItem}
          // refreshControl={
          //   <RefreshControl
          //     refreshing={false}
          //     onRefresh={debounce(() => onRefresh(), 100, {
          //       trailing: false,
          //       leading: true,
          //     })}
          //   />
          // }
          // ListFooterComponent={footerItem}
          // style={{paddingHorizontal: ownSize.screen}}
          // contentContainerStyle={styles.flatlist}
          // horizontal={true}
          // showsHorizontalScrollIndicator={false}
        />
          
        
      </VStack>
      {/* </ScrollView> */}
    </Box>
  );
}

export default SubjectsTop;

SubjectsTop.defaultProps = {
  route: {},
};

SubjectsTop.propTypes = {
  route: PropTypes.shape({}),
};

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
});