/* eslint-disable react/jsx-no-bind */
import React from 'react';
import {View,  Image, StyleSheet} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {PropTypes} from 'prop-types';
import {responsiveWidth as wp, responsiveHeight as hp} from 'react-native-responsive-dimensions';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';

import {saveWalktrough} from 'redux/constantReducer';

function WalktroughPage () {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();

  const slides = [
    {
      key: 1,
      title: '',
      text: '',
      image: require('assets/img/1.jpg'),
      backgroundColor: '#ffffff',
    },
    {
      key: 2,
      title: '',
      text: '',
      image: require('assets/img/2.jpg'),
      backgroundColor: '#ffffff',
    },
    {
      key: 3,
      title: '',
      text: '',
      image: require('assets/img/3.jpg'),
      backgroundColor: '#ffffff',
    },
    {
      key: 4,
      title: '',
      text: '',
      image: require('assets/img/4.jpg'),
      backgroundColor: '#ffffff',
    },
  ];

  const renderItem = ({item}) => (
    <View style={styles.container}>
      <Image
        source={item.image}
        style={styles.imgContainer}
      />
    </View>
  );

  const onDone = async () => {
    // await AsyncStorage.setItem('isWalktrough', 'false');
    dispatch(saveWalktrough(false));
    navigation.navigate('XpertTab');
  };

  return (
    <View style={styles.container}>
      <AppIntroSlider
        renderItem={renderItem}
        data={slides}
        dotClickEnabled={true}
        onDone={onDone}
        doneLabel={'Get Started'}
      />
    </View>
  );
}

WalktroughPage.defaultProps = {

};

WalktroughPage.propTypes = {
  datasource: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  labelSignUp: PropTypes.string,
  labelSignin: PropTypes.string,
  onSignUp: PropTypes.func,
  onSignIn: PropTypes.func,
  onHome: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgContainer: {
    width: wp(100), 
    height: hp(100), 
    resizeMode: 'cover'
  },
});

export default React.memo(WalktroughPage);