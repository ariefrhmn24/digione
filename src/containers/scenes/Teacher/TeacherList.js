/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {Box, VStack, ScrollView, Avatar, Divider, FlatList, Text, HStack, Image, Center, View, Skeleton} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {StyleSheet, RefreshControl, Pressable, Platform} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {isEmpty, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import SwipeCards from 'react-native-swipe-cards-deck';

import {getTeacherList} from 'services/teacher.thunks';
import {saveTeacherSelected} from 'redux/teacherReducer';
import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import FilterHorizontal from 'components/molecules/FilterHorizontal';
import ProfileCard from 'components/molecules/ProfileCard';
import TeacherCard from 'components/molecules/TeacherCard';

import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function TeacherList() {
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const teacherRdx = useSelector((state) => state.teacherRdx);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [teacherList, setTeacherList] = useState(false);
  console.log('teacherRdx', teacherRdx);

  useEffect(() => {
    if (!isEmpty(teacherRdx.teacherList)) {
      const content = result(teacherRdx, 'teacherList.content', 'Skool');
      setTeacherList(content);
    }
  }, [teacherRdx]);

  useEffect(() => {
    async function initialize() {
      await fetchTeacherList();
    }
    initialize();
  }, []);

  const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    fetchTeacherList();
    wait(1000).then(() => {
      setRefreshing(false);
    });
  }, []);

  const fetchTeacherList = () => {
    setIsLoading(true);
    const payload = {};
    dispatch(getTeacherList(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const goToTeacherDetails = ({item, index}) => () => {
    dispatch(saveTeacherSelected(item));
    navigation.navigate('TeacherDetailPage');
  };

  const renderTeacher = ({item, index}) => {
    const itemName = result(item, 'name', '');
    const itemSubject = result(item, 'subject', '');
    const itemImage = result(item, 'fileImage', '');
    const currentStudent = result(item, 'currentStudent', '');
    return (
      <VStack flex='1' mx='5' mb='5'>
        <Pressable onPress={goToTeacherDetails({item, index})}>
          <TeacherCard 
            name={itemName}
            subject={itemSubject}
            seats={currentStudent}
            url={itemImage} />
        </Pressable>
      </VStack>
    );
  };

  const emptyItem = () => (
    <Box mb='5' mx='5'>
      <VStack space='5'>
        <Skeleton height='55px' borderRadius={10} />
        <Skeleton height='55px' borderRadius={10} />
      </VStack>
    </Box>
  );

  return (
    <SafeAreaView header='blue'>
      <Box flex='1' bg='white'>
        <HStack m='5' space='5'>
          <HStack justifyContent='space-between' alignItems='center'>
            <Text fontSize='lg' fontWeight='semibold'>{'Thirst for knowledge'}</Text>
          </HStack>
        </HStack>
        <FlatList
          pt='1'
          data={teacherList}
          renderItem={renderTeacher}
          keyExtractor={(item) => item.id}
          ListEmptyComponent={emptyItem}
        />
        {/* <Divider width='100%' m='2.5' bg='white' /> */}
         
      </Box>
    </SafeAreaView>
  );
}

export default TeacherList;

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
  linearGradient: {
    height: 100,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    // position: 'absolute',
    // top: 0,
    // width: '100%'
  },
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    
  },
});