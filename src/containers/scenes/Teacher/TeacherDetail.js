/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {Box, VStack, ScrollView, Button, useDisclose, Actionsheet, Text, FormControl, Input} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {StyleSheet, RefreshControl, Linking, Platform} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {isEmpty, result, debounce} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import SwipeCards from 'react-native-swipe-cards-deck';

import {getTeacherDetail} from 'services/teacher.thunks';
import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import FilterHorizontal from 'components/molecules/FilterHorizontal';
import ProfileCard from 'components/molecules/ProfileCard';
import TeacherCard from 'components/molecules/TeacherCard';

import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function FormLogin({onLogin, onForgotPassword, onForgotUser, isLoading}) {
  const {t, i18n} = useTranslation();
  const {control, handleSubmit, errors} = useForm();

  const [show, setShow] = React.useState(false);
  const handleShow = () => setShow(!show);

  const onPressLogin = (data) => {
    onLogin(data);
  };

  const onPressForgotUser = () => {
    onForgotUser();
  };
  
  const onPressForgotPassword = () => {
    onForgotPassword();
  };

  return (
    <Box width='100%' p='3'>
      <VStack space={4}>
        <FormControl>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                onBlur={onBlur }
                placeholder={'Full Name'}
                onChangeText={(val) => onChange(val)}
                value={value}
              />
            )}
            name='fullname'
            defaultValue=''
          />
        </FormControl>
        <FormControl>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                type={'text'}
                onBlur={onBlur}
                placeholder={'Phone Number'}
                onChangeText={(val) => onChange(val)}
                value={value}
               
              />
            )}
            name='phonenumber'
            defaultValue=''
          />
        </FormControl>
        <FormControl>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                type={'text'}
                onBlur={onBlur}
                placeholder={'Email Address'}
                onChangeText={(val) => onChange(val)}
                value={value}
               
              />
            )}
            name='emailaddress'
            defaultValue=''
          />
        </FormControl>
        <Button 
          onPress={handleSubmit(onPressLogin)} 
          // isLoading={isLoading} 
          // isLoadingText='Submitting'
          bg='brand.500' 
          py={4}
          borderRadius={10}
        >
          {'Book Now'}
        </Button>
        <Box alignSelf='center' flexDirection='row'>
          <Text textAlign='center'>{'By clicking Book Now, you’ll be directed to Whatsapp so you can discuss further if you have any questions.'}</Text>
        </Box>
      </VStack>
    </Box>
  );
}

FormLogin.propTypes = {
  onLogin: PropTypes.func,
  onForgotPassword: PropTypes.func,
  onForgotUser: PropTypes.func,
  isLoading: PropTypes.bool
};

function TeacherDetail() {
  const {isOpen, onOpen, onClose} = useDisclose();
  const {t, i18n} = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const toast = myToast();
  const teacherRdx = useSelector((state) => state.teacherRdx);
  const teacherSelected = teacherRdx.teacherSelected;
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [teacherContent, setTeacherContent] = useState([]);

  useEffect(() => {
    if (!isEmpty(teacherRdx.teacherDetail)) {
      const content = result(teacherRdx, 'teacherDetail', []);
      setTeacherContent(content);
    }
  }, [teacherRdx]);

  useEffect(() => {
    async function initialize() {
      await fetchTeacherDetail();
    }
    initialize();
  }, []);

  const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    fetchTeacherDetail();
    wait(1000).then(() => {
      setRefreshing(false);
    });
  }, []);

  const fetchTeacherDetail = () => {
    setIsLoading(true);
    const payload = teacherSelected;
    dispatch(getTeacherDetail(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const renderItem = (item, index) => (
    <Box>
      <Text fontSize='md'>{item.title}</Text>
      <Text fontSize='md' fontWeight='semibold'>{item.val}</Text>
    </Box>
  );

  const onLogin = (data) => {
    const fullname = result(data, 'fullname', '').split(' ').join('%20');
    const textUrl = `Halo! perkenalkan nama saya ${fullname}. Saya tertarik dengan kursus yang terdaftar pada aplikasi Skool. Saya ingin tahu informasi lebih lanjut untuk mendaftar`;
    const joinSpace = textUrl.split(' ').join('%20');
    const url = `https://wa.me/6287889128755?text=${joinSpace}`;
    Linking.openURL(url);

    // Linking.canOpenURL(url).then((supported) => {
    //   if (supported) {
    //     return Linking.openURL(url).catch(() => null);
    //   }
    // });
  };

  

  const onForgotUser = () => {
    navigation.navigate('ForgotUserIDScreen');
  };

  const onForgotPassword = () => {
    navigation.navigate('ForgotPasswordScreen');
  };

  return (
    <SafeAreaView header='blue'>
      <Box flex='1' bg='white'>
        <LinearGradient
          colors={['#0042A6', '#507DF1']}
          style={styles.linearGradient}
          useAngle={true}
          angle={104}
        />
        <ScrollView refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor='white' />
        }>
          <Box m='5'>
            <ProfileCard 
              title={teacherSelected.name}
              subTitle={teacherSelected.subject}
              subTitle2={''}
              url={teacherSelected.fileImage}
              rightIcon={false} />
          </Box>
          <Box p='5' bg='white'>
            <Text fontSize='lg' fontWeight='bold'>{'Things you can expect from me'}</Text>
          </Box>
          <VStack space='3' mx='5'>
            {teacherContent.map((item, i) => renderItem(item, i))}
          </VStack>
          <Box p='5'>
            <Button 
              onPress={onOpen}
              // isLoading={isLoading} 
              // isLoadingText='Submitting'
              bg='brand.500' 
              py={4}
              borderRadius={10}
            >
              {'I\'m Interested'}
            </Button>
          </Box>
        </ScrollView>
        <Actionsheet isOpen={isOpen} onClose={onClose}>
          <Actionsheet.Content>
            <Text p='3' alignSelf='flex-start' fontSize='lg'>{'Help us to fill in your details'}</Text>
            <FormLogin
              onLogin={debounce((v) => onLogin(v), 100, {
                trailing: false,
                leading: true,
              })}
              onForgotPassword={debounce(() => onForgotPassword(), 100, {
                trailing: false,
                leading: true,
              })}
              onForgotUser={debounce(() => onForgotUser(), 100, {
                trailing: false,
                leading: true,
              })}
              isLoading={isLoading}
            />
          </Actionsheet.Content>
        </Actionsheet>
      </Box>
    </SafeAreaView>
  );
}

export default TeacherDetail;

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
  linearGradient: {
    height: 100,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    position: 'absolute',
    top: 0,
    width: '100%'
  },
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    
  },
});