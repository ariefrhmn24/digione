/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback, useLayoutEffect} from 'react';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView} from 'native-base';
import {StyleSheet, RefreshControl} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect, useIsFocused} from '@react-navigation/native';
import {isEmpty, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import Organism from 'containers/organism/Account/AccountPage';
import {getProfile, postLogout} from 'services/account.thunks';
import {myToast} from 'components/Toast';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function AccountPage() {
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const toast = myToast();
  const accountRdx = useSelector((state) => state.accountRdx);
  const constantRdx = useSelector((state) => state.constantRdx);
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [name, setName] = useState(result(accountRdx, 'name', 'Skool'));
  const [email, setEmail] = useState(result(accountRdx, 'email', 'skool.edtech@gmail.com'));
  const [mobileNumber, setMobileNumber] = useState(result(accountRdx, 'mobileNumber', ''));
  const [rdxLangDesc, setRdxLangDesc] = useState(result(constantRdx, 'lang.desc', 'Indonesia'));
  const isFocused = useIsFocused();

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    fetchProfile();
    wait(1000).then(() => {
      setRefreshing(false);
    });
  }, []);

  useEffect(() => {
    const setlang = result(constantRdx, 'lang.desc', 'Indonesia');
    setRdxLangDesc(setlang);
  }, [isFocused]);

  useEffect(() => {
    if (!isEmpty(accountRdx.profile)) {
      const namerdx = result(accountRdx, 'profile.name', 'Skool');
      const emailrdx = result(accountRdx, 'profile.email', '');
      const mobileNumberrdx = result(accountRdx, 'profile.mobileNumber', '');
      setName(namerdx);
      setEmail(emailrdx);
      setMobileNumber(mobileNumberrdx);
    }
  }, [accountRdx]);

  useEffect(() => {
    async function initialize() {
      await fetchProfile();
    }
    initialize();
  }, []);

  const fetchProfile = () => {
    setIsLoading(true);
    const payload = {};
    dispatch(getProfile(payload, 
      (callback) => {
        const responseMsg = result(callback, 'response.data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const goToLanguage = () => {
    navigation.navigate('LanguagesPage');
  };

  const menuGeneral = [
    {
      label: 'txtLanguage',
      subLabel: rdxLangDesc,
      onPress: goToLanguage
    },
    {
      label: 'txtNotifSetting',
      subLabel: '',
      onPress: () => ({}),
    },
    {
      label: 'txtLocation',
      subLabel: '',
      onPress: () => ({}),
    }
  ];

  const onLogout = () => {
    const payload = {
    };
    dispatch(postLogout(payload, 
      (callback) => {
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
    
  };
 
  return (
    <SafeAreaView header='blue'>
      <Organism 
        refreshing={refreshing}
        onRefresh={onRefresh}

        name={name}
        email={email}
        mobileNumber={mobileNumber}

        menuGeneral={menuGeneral}

        onLogout={onLogout}
      />
    </SafeAreaView>
  );
}

export default AccountPage;

AccountPage.defaultProps = {
  name: 'Skool',
};

AccountPage.propTypes = {
  name: PropTypes.string,
};

const styles = StyleSheet.create({
  linearGradient: {
    height: 100,
  },
});