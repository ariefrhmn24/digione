/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useRef} from 'react';
import {Box, VStack, Avatar, Text, HStack, Divider, Spacer, Link, Pressable, ScrollView, Radio} from 'native-base';
import {StyleSheet, RefreshControl} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {isEmpty, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {myToast} from 'components/Toast';

import Hoc from 'components/Hoc';
import {saveLang} from 'redux/constantReducer';
const SafeAreaView = Hoc;

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function LanguagePage() {
  const {t, i18n} = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const toast = myToast();
  const constantRdx = useSelector((state) => state.constantRdx);
  const [rdxLang] = useState(result(constantRdx, 'lang.id', 'id'));
  const [value, setValue] = useState(rdxLang);
  const ref = useRef(null);

  useEffect(() => {
    let setLang = {
      id: '',
      desc: ''
    };
    if (value === 'id') {
      setLang = {
        id: 'id',
        desc: 'Indonesia'
      };
      dispatch(saveLang(setLang));
      i18n.changeLanguage('id');
    } else {
      setLang = {
        id: 'en',
        desc: 'English'
      };
      dispatch(saveLang(setLang));
      i18n.changeLanguage('en');
    }
  }, [value]);

  return (
    <SafeAreaView header='blue'>
      <Box flex='1' bg='bg.500'>
        <Box bg='white' px='5' py='3'>
          <Radio.Group 
            name='myRadioGroup' 
            accessibilityLabel='radio-language' 
            value={value} 
            onChange={(nextValue) => {
              setValue(nextValue);
            }}>
            <Radio value='id' my='2'>
              <Text ml='5' fontSize='lg'>{'Indonesia'}</Text>
            </Radio>
            <Divider width='100%' bg='bg.500' />
            <Radio value='en' my='2' ref={ref}>
              <Text ml='5' fontSize='lg'>{'English'}</Text>
            </Radio>
          </Radio.Group>    
        </Box>
      </Box>
    </SafeAreaView>
  );
}

export default LanguagePage;

LanguagePage.defaultProps = {
  name: 'Skool',
};

LanguagePage.propTypes = {
  name: PropTypes.string,
};

const styles = StyleSheet.create({
  linearGradient: {
    height: 100,
  },
});