/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Heading, Text, View, KeyboardAvoidingView} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';

import Organism from 'containers/organism/Authorization/SignupEmail';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';
import {postRegister} from 'services/signup.thunks';
import {saveEmail} from 'redux/signupReducer';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function SignupEmail() {
  const [isLoading, setIsLoading] = useState(false);
  const {signIn} = React.useContext(AuthContext);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const signupRdx = useSelector((state) => state.signupRdx);
  const [rdxEmail, setRdxEmail] = useState(result(signupRdx, 'email', ''));

  const goToSignupOtp = (data) => {
    console.log('data', data);
    dispatch(saveEmail(data.email));

    setIsLoading(true);
    const payload = {};
    dispatch(postRegister(payload, 
      (callback) => {
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };
  
  return (
    <SafeAreaView header='light'>
      <KeyboardAvoidingView 
        keyboardVerticalOffset={Platform.OS === 'ios' ? 140 : 75}
        flex='1'
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <Organism 
          goToSignupOtp={goToSignupOtp}
          isLoading={isLoading}

          rdxEmail={rdxEmail}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

export default SignupEmail;