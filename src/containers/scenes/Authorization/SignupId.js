/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Actionsheet, Text, View, KeyboardAvoidingView, useDisclose} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';
import Moment from 'moment';

import Organism from 'containers/organism/Authorization/SignupId';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';
import {saveEmail} from 'redux/signupReducer';
import {postSetName} from 'services/signup.thunks';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function SignupId() {
  const [isLoading, setIsLoading] = useState(false);
  const {signIn} = React.useContext(AuthContext);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const loginRdx = useSelector((state) => state.loginRdx);

  const [date, setDate] = useState(new Date(1598051730000));
  const [show, setShow] = useState(false);

  const goToSignupCompleted = (data) => {
    // dispatch(saveEmail(data.email));
    // navigation.navigate('SignupCompletedPage');
    setIsLoading(true);
    const payload = {
      name: data.name,
      birthDate: date
    };
    dispatch(postSetName(payload, 
      (callback) => {
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };
  
  return (
    <SafeAreaView header='light'>
      <KeyboardAvoidingView 
        keyboardVerticalOffset={Platform.OS === 'ios' ? 140 : 75}
        flex='1'
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <Organism 
          goToSignupCompleted={goToSignupCompleted}
          isLoading={isLoading}
          date={date}
          setDate={setDate}

        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

export default SignupId;