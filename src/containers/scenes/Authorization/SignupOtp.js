/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Heading, Text, View, KeyboardAvoidingView} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt, maskingEmail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';

import Organism from 'containers/organism/Authorization/SignupOtp';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';
import {postVerifyOtp, postResendOtp} from 'services/signup.thunks';
import {saveLogin} from 'redux/loginReducer';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function SignupOtp() {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const signupRdx = useSelector((state) => state.signupRdx);
  const [maskedEmail, setMaskedEmail] = useState('');

  // useEffect(() => {
  //   const postVerify = result(signupRdx, 'postVerify', {});
  //   const isMaxVerify = postVerify.isMaxVerify;
  //   if (isMaxVerify) {
  //     toast(responseMsg);
  //   }
  // }, [signupRdx]);

  useEffect(() => {
    async function initialize() {
      const rdxEmail = result(signupRdx, 'email', '');
      const handleMaskingEmail = maskingEmail(rdxEmail);
      setMaskedEmail(handleMaskingEmail);
    }

    initialize();
  }, []);

  const onVerifyOtp = (data) => {
    setIsLoading(true);
    const payload = {
      otp: data
    };
    dispatch(postVerifyOtp(payload, 
      (callback) => {
        console.log('callback', callback);
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const onResendOtp = () => {
    setIsLoading(true);
    const payload = {};
    dispatch(postResendOtp(payload, 
      (callback) => {
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };
  
  return (
    <SafeAreaView header='light'>
      <Organism 
        isLoading={isLoading}

        maskedEmail={maskedEmail}
        onVerifyOtp={onVerifyOtp}
        onResendOtp={onResendOtp}
      />
    </SafeAreaView>
  );
}

export default SignupOtp;