/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Heading, Text, View, KeyboardAvoidingView} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt, maskingEmail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';

import Organism from 'containers/organism/Authorization/SignupCompleted';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';
import {saveLogin} from 'redux/loginReducer';

import Hoc from 'components/Hoc';
import {resetSignupRdx} from 'redux/signupReducer';
const SafeAreaView = Hoc;

function SignupCompleted() {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const signupRdx = useSelector((state) => state.signupRdx);

  const goToExplore = () => {
    dispatch(resetSignupRdx());
    navigation.navigate('ExplorePage');
  };

  useEffect(() => {
    async function initialize() {
    }

    initialize();
  }, []);

  return (
    <SafeAreaView header='light'>
      <Organism 
        goToExplore={goToExplore}
        isLoading={isLoading}

      />
    </SafeAreaView>
  );
}

export default SignupCompleted;