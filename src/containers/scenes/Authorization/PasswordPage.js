/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, KeyboardAvoidingView, Text, View} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';

import Organism from 'containers/organism/Authorization/PasswordPage';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/signup.thunks';

import Hoc from 'components/Hoc';
const SafeAreaView = Hoc;

function PasswordPage() {
  const [isLoading, setIsLoading] = useState(false);
  const {signIn} = React.useContext(AuthContext);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();
  const toast = myToast();
  const loginRdx = useSelector((state) => state.loginRdx);

  const goToSignupId = (data) => {
    console.log('data', data);
    setIsLoading(true);
    const payload = {
      password: doEncrypt(data.password),
      confirmPassword: doEncrypt(data.confirmPassword),
    };
    dispatch(postSetPassword(payload, 
      (callback) => {
        const responseMsg = result(callback, 'data.message', 'Please try again');
        toast(responseMsg);
      },
      (final) => setIsLoading(final)
    ));
  };

  const onRemove = async () => {
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('isWalktrough');
  };

  const onForgotPassword = () => {
    // navigation.navigate('ForgotPasswordScreen');
  };
  
  return (
    <SafeAreaView header='light'>
      <Organism 
        isLoading={isLoading}
        goToSignupId={goToSignupId}
        onForgotPassword={onForgotPassword}

      />
    </SafeAreaView>
  );
}

export default PasswordPage;