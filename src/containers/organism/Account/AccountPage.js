/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback} from 'react';
import {Box, VStack, Text, HStack, Divider, Pressable, ScrollView} from 'native-base';
import {StyleSheet, RefreshControl} from 'react-native';
import {PropTypes} from 'prop-types';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';

import ProfileCard from 'components/molecules/ProfileCard';
import MenuList from 'components/molecules/MenuList';

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function AccountPage({
  refreshing,
  onRefresh,
  name,
  email,
  mobileNumber,
  menuTitle,
  menuGeneral,
  onLogout
}) {
  const {t, i18n} = useTranslation();
  
  return (
    <Box flex='1' bg='bg.500'>
      <LinearGradient
        // start={{x: 0, y: 0}} end={{x: 1.5, y: 2}}
        // locations={[0, 0.5, 0.6]}
        colors={['#0042A6', '#507DF1']}
        style={styles.linearGradient}
        useAngle={true}
        angle={104}
      />
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
          tintColor='white'
          // colors='white'
        />
      }>
        <Box m='5'>
          <ProfileCard 
            title={name}
            subTitle={email}
            subTitle2={mobileNumber} />
        </Box>
        <MenuList 
          menuTitle={menuTitle}
          menuData={menuGeneral} />

        <Divider width='100%' p='2' bg='bg.500' />
        
        {/* <MenuList 
          menuTitle={menuTitle}
          menuGeneral={menuGeneral} /> */}
        {/* 
        <Divider width='100%' p='2' bg='bg.500' /> */}
          
        <VStack p='5' bg='white' space='3'>
          <Pressable onPress={onLogout}>
            <HStack justifyContent='space-between'>
              <Text fontWeight='bold'>{t('txtLogout')}</Text>
            </HStack>
          </Pressable>
        </VStack>
        
      </ScrollView>
    </Box>
  );
}

export default AccountPage;

AccountPage.defaultProps = {
  refreshing: false,
  onRefresh: () => {},
  name: 'Skool',
  email: 'skool.edtech@gmail.com',
  mobileNumber: '-',
  menuTitle: 'txtGeneral',
  menuData: [
    {
      label: 'txtTnC',
      onPress: () => {},
    },
    {
      label: 'txtHelpCenter',
      onPress: () => {},
    },
    {
      label: 'txtSettings',
      onPress: () => {},
    }
  ]
};

AccountPage.propTypes = {
  refreshing: PropTypes.bool,
  onRefresh: PropTypes.func,
  name: PropTypes.string,
  email: PropTypes.string,
  mobileNumber: PropTypes.string,
  menuTitle: PropTypes.string,
  menuData: PropTypes.arrayOf(PropTypes.shape({})),
};

const styles = StyleSheet.create({
  linearGradient: {
    height: 100,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    position: 'absolute',
    top: 0,
    width: '100%'
  },
});