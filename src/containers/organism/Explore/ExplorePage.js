/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {Box, VStack, ScrollView, AspectRatio, Divider, FlatList, Text, HStack, Image, Center, View, Spacer, Switch} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {StyleSheet, RefreshControl, Pressable, Dimensions} from 'react-native';
import {PropTypes} from 'prop-types';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {isEmpty, result} from 'lodash';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import LinearGradient from 'react-native-linear-gradient';
import SwipeCards from 'react-native-swipe-cards-deck';
import {SvgXml} from 'react-native-svg';

import {getTeacherList} from 'services/teacher.thunks';
import {doEncrypt} from 'helpers';
import {apiLogin} from 'services/api';
import {myToast} from 'components/Toast';
import FilterHorizontal from 'components/molecules/FilterHorizontal';
import ProfileCard from 'components/molecules/ProfileCard';
import SignupCard from 'components/molecules/SignupCard';
import PopularClasses from 'components/molecules/PopularClasses';

import IcProductTutors from 'assets/svg/ic-product-tutors.svg';
import IcProductCourses from 'assets/svg/ic-product-courses.svg';
import IcProductWorkshops from 'assets/svg/ic-product-workshops.svg';
import IcProductCommunity from 'assets/svg/ic-product-community.svg';
import IcChevronRight from 'assets/svg/ic-chevron-right.svg';
import IcBgSignUp from 'assets/svg/bg-signup.svg';

function ProductSection ({goToTutors}) {
  useEffect(() => {
    setTimeout(() => {
      
    }, 1000);
  }, []);


  return (
    <Box>
      <HStack justifyContent='space-between' alignItems='center'>
        <Text fontSize='lg' fontWeight='semibold'>{'Choose as you like'}</Text>
      </HStack>
      <Divider width='100%' p='2' bg='white' />
      <VStack space='5'>
        <HStack space='5'>
          <Pressable flex={1} onPress={goToTutors}>
            <VStack alignItems='center' p='3' space='3' borderRadius={10} shadow='2' bg='white'>
              <IcProductTutors />
              <Text bold>{'Tutors'}</Text>
              <Text textAlign='center' fontSize='xs'>{'Find qualified tutors to your preferred interests'}</Text>
              <Text fontWeight='semibold' underline>{'Explore >'}</Text>
            </VStack>
          </Pressable>
          <Pressable flex={1}>
            <VStack space='3' alignItems='center' p='3' borderRadius='10' shadow='2' bg='white'>
              <IcProductCourses />
              <Text bold>{'Courses'}</Text>
              <Text textAlign='center' fontSize='xs'>{'Explore courses to your preferred interests'}</Text>
              <Text fontWeight='semibold'>{'Coming Soon!'}</Text>
            </VStack>
          </Pressable>
        </HStack>
        <HStack flex='1' space='5'>
          <VStack flex='1' space='3' alignItems='center' p='3' borderRadius='10' shadow='2' bg='white'>
            <IcProductWorkshops />
            <Text bold>{'Workshops'}</Text>
            <Text textAlign='center' fontSize='xs'>{'Variety of workshops to your preferred interests'}</Text>
            <Text fontWeight='semibold' underline>{'Coming Soon!'}</Text>
          </VStack>
          <VStack flex='1' space='3' alignItems='center' p='3' borderRadius='10' shadow='2' bg='white'>
            <IcProductCommunity />
            <Text bold>{'Community'}</Text>
            <Box>
              <Text textAlign='center' fontSize='xs'>{'Find your people.'}</Text>
              <Text textAlign='center' fontSize='xs'>{'Connect & Communicate.'}</Text>
            </Box>
            <Text fontWeight='semibold'>{'Coming Soon!'}</Text>
          </VStack>
        </HStack>
      </VStack>
    </Box>
  );
}

function Card({data, handleYup, handleNope}) {
  return (
    <Center p='5' bg={data.backgroundColor} shadow='2' borderRadius='10'>
      <Box bg='bg.500' borderRadius='20' px='2' py='1'>
        <Text>{data.subject}</Text>
      </Box>
      <Box my='4'>
        <Text fontSize='md' fontWeight='semibold'>{data.text}</Text>
      </Box>
      <Box width='100%'>
        <HStack alignSelf='center' space='5'>
          <Pressable onPress={handleNope(data)}>
            <Box borderWidth='1' borderRadius='10' borderColor='blueGray.300' py='2' px='4'>
              <Text>{data.labelLeft}</Text>
            </Box>
          </Pressable>
          <Pressable onPress={handleYup(data)}>
            <Box borderWidth='1' borderRadius='10' borderColor='blueGray.300' py='2' px='4'>
              <Text>{data.labelRight}</Text>
            </Box>
          </Pressable>
        </HStack>
      </Box>
    </Center>
  );
}

function StatusCard({text}) {
  return (
    <Center>
      <Text fontSize='md'>{text}</Text>
    </Center>
  );
}

function QotdSection () {
  const [cards, setCards] = useState();
  let bsCard = null;

  const handleYup = (card) => () => {
    bsCard.swipeYup();
    console.log(`Yup for ${card.text}`);
    return true; // return false if you wish to cancel the action
  };
  const handleNope = (card) => () => {
    bsCard.swipeNope();
    console.log(`Nope for ${card.text}`);
    return true;
  };

  useEffect(() => {
    setTimeout(() => {
      setCards([
        {subject: 'English', text: 'Putri ... a letter for you.', backgroundColor: 'white', labelLeft: 'Wrote', labelRight: 'Writes'},
        {subject: 'Feedback', text: 'Do you like our app?', backgroundColor: 'white', labelLeft: 'Not yet', labelRight: 'Sure!'},
      ]);
    }, 1000);
  }, []);

  const actions = {
    nope: {show: false, onAction: handleNope}, 
    yup: {show: false, onAction: handleYup},
  };

  return (
    <Box mx='5'>
      <HStack justifyContent='space-between' alignItems='center'>
        <Text fontSize='lg' fontWeight='semibold'>{'Question of the day'}</Text>
        {/* <Text>{'See all'}</Text> */}
      </HStack>
      <Spacer mb='5' />
      <Box>
        <SwipeCards
          ref={(ref) => (bsCard = ref)}
          style={{
            alignItems: 'stretch',
            flexGrow: 1,
          }}
          cards={cards}
          renderCard={(cardData) => <Card data={cardData} handleYup={handleYup} handleNope={handleNope} />}
          keyExtractor={(cardData) => String(cardData.text)}
          renderNoMoreCards={() => <StatusCard text='Thank you!' />}
          actions={actions}
          hasMaybeAction={false}
          stackOffsetY={10}
          stackOffsetX={10}
          // If you want a stack of cards instead of one-per-one view, activate stack mode
          // stack={true}
          stackDepth={3}
        />
      </Box>
    </Box>
  );
}

function ExplorePage({
  isIos,
  refreshing,
  onRefresh,
  greetings,
  goToTutors,
  goToSignup,
  rdxIsLogin
}) {
  const {t, i18n} = useTranslation();
  const originalWidth = '327';
  const originalHeight = '50';

  const gradient = () => (
    <LinearGradient
      colors={['#0042A6', '#507DF1']}
      style={styles.linearGradient}
      useAngle={true}
      angle={104}
    />
  );
  
  const aspectRatio = originalWidth / originalHeight;
  const windowWidth = Dimensions.get('window').width;

  return (
    <Box flex={1} 
      bg={{
        linearGradient: {
          colors: ['#0042A6', '#507DF1'],
          start: [0.0, 1.0],
          end: [1.4, 0.9]
        }
      }}
    >
      <ScrollView 
        bounces={true} 
        showsVerticalScrollIndicator={false} 
        style={{backgroundColor: isIos ? gradient : 'white'}}
        contentContainerStyle={{backgroundColor: 'white'}}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor='white'
          />
        }>
        <VStack space='5'>
          <LinearGradient
            colors={['#0042A6', '#507DF1']}
            style={styles.linearGradient}
            useAngle={true}
            angle={104}
            // start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: -1.4}}
          >
            <HStack justifyContent='space-between' mx='5' alignItems='center'>
              <VStack>
                <Text fontSize='2xl' color='white' fontWeight='semibold'>{greetings}</Text>
                <Text fontSize='2xl' color='white' fontWeight='semibold'>{t('txtEnthusiast')}</Text>
              </VStack>
            </HStack>
          </LinearGradient>

          { !rdxIsLogin &&
          <Box mx='5'>
            <SignupCard 
              originalWidth={originalWidth}
              originalHeight={originalHeight}
              title={t('txtSignupTitle')}
              description={t('txtSignupDesc')}
              onPress={goToSignup}
            />
          </Box> 
          }

          <Box mx='5'>
            <PopularClasses 
              title={t('txtPopularClasses')}
              classes={'The art of intermittent fasting'}
              author={'Pupu funky'}
            />
          </Box>
          <Divider width='100%' p='2' bg='bg.500' />

          <Box mx='5'>
            <ProductSection 
              goToTutors={goToTutors}
            />
          </Box>
          <Divider width='100%' p='2' bg='bg.500' />
          <QotdSection />
          <Divider width='100%' p='2' bg='bg.500' />
        </VStack>
      </ScrollView>
    </Box>
  );
}

export default ExplorePage;

ExplorePage.defaultProps = {
  greetings: 'Good Morning,',
  isIos: true,
  refreshing: false,
  onRefresh: () => {},
  goToTutors: () => {},
};

ExplorePage.propTypes = {
  greetings: PropTypes.string,
  isIos: PropTypes.bool,
  refreshing: PropTypes.bool,
  onRefresh: PropTypes.func,
  goToTutors: PropTypes.func,
};

const styles = StyleSheet.create({
  textSuperBold: {
    fontFamily: 'Roboto-Bold',
  },
  linearGradient: {
    height: 100,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    // position: 'absolute',
    // top: 0,
    // width: '100%'
  },
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    
  },
});