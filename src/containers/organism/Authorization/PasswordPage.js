/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Heading, Text, View, HStack} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {doEncrypt} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';

import FormPassword from 'components/molecules/FormPassword';
import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';

function PasswordPage({
  isLoading,
  onForgotPassword,
  goToSignupId
}) {
  const {t, i18n} = useTranslation();
  const {control, handleSubmit, errors, watch, getValues, setValue} = useForm();
  const {password} = getValues();
  const watchPassword = watch('password', '');

  const [error, setError] = useState('');
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const handleShow = () => setShow(!show);
  const handleShow2 = () => setShow2(!show2);

  const onPressLogin = (data) => {
    if (data.password !== data.confirmPassword) {
      setError('Password tidak sesuai');
      // goToSignupId(data);
    } else {
      setError('');
      goToSignupId(data);
    }
    // setTimeout(() => {
    //   setValue('password', '');
    // }, 1000);
  };

  return (
    <Box p='5' flex='1' justifyContent={'space-between'} bg='white'>
      <VStack space={7}>
        <Text fontWeight='semibold' fontSize='3xl'>{t('txtCreatePassword')}</Text>
        <Text>{t('txtCreatePassword2')}</Text>
        <FormPassword
          control={control}
          errors={errors}
          setValue={setValue}
          error={error}
          setError={setError}
          show={show}
          show2={show2}
          handleShow={handleShow}
          handleShow2={handleShow2}
          watchPassword={watchPassword}
          onForgotPassword={debounce(() => onForgotPassword(), 100, {
            trailing: false,
            leading: true,
          })}
        />
      </VStack>
      <Button 
        onPress={handleSubmit(onPressLogin)} 
        isLoading={isLoading} 
        isLoadingText='Submitting'
        bg='brand.500' 
        py={4}
        borderRadius={10}
      >
        {t('txtContinue')}
      </Button>
    </Box>
  );
}

export default PasswordPage;

PasswordPage.defaultProps = {
  isLoading: false,
  goToSignupId: () => {},
  onForgotPassword: () => {},
};

PasswordPage.propTypes = {
  isLoading: PropTypes.bool,
  goToSignupId: PropTypes.func,
  onForgotPassword: PropTypes.func,
};