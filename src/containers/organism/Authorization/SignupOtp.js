/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, ScrollView, Text} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {regexMail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {StyleSheet} from 'react-native';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {ownColor} from 'theme/color';
import Countdown from 'components/molecules/Countdown';

function SignupOtp({
  maskedEmail,
  onVerifyOtp,
  onResendOtp
}) {
  const {t, i18n} = useTranslation();
  const CELL_COUNT = 6;

  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  useEffect(() => {
    const valueLength = value.length;
    if (valueLength === 6) {
      onVerifyOtp(value);
    }
  }, [value]);

  
  return (
    <Box p='5' flex={1} justifyContent={'space-between'} bg='white'>
      <VStack space={7}>
        <Text fontWeight='semibold' fontSize='3xl'>{t('txtEmailVerify')}</Text>
        <Text>{t('txtEmailVerify2')} {maskedEmail}</Text>
        <CodeField
          ref={ref}
          {...props}
          value={value}
          onChangeText={setValue}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType='number-pad'
          textContentType='oneTimeCode'
          renderCell={({index, symbol, isFocused}) => (
            <Text
              key={index}
              style={[styles.cell, isFocused && styles.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}
              size={'xl'}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
        <Countdown onResendOtp={onResendOtp}/>
      </VStack>
    </Box>
  );
}

export default SignupOtp;

SignupOtp.defaultProps = {
  maskedEmail: '',
  onVerifyOtp: () => {},
  onResendOtp: () => {},
};

SignupOtp.propTypes = {
  maskedEmail: PropTypes.string,
  onVerifyOtp: PropTypes.func,
  onResendOtp: PropTypes.func,
};

const styles = StyleSheet.create({
  codeFieldRoot: {
    // height: 50,
    justifyContent: 'center',
  },
  focusCell: {
    borderColor: ownColor.secondary,
  },
  cell: {
    height: 65,
    width: '13%',
    lineHeight: 65,
    textAlign: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: ownColor.primary10,
    color: '#3759b8',
    backgroundColor: '#fff',
    marginHorizontal: 5,

    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.22,
    // shadowRadius: 2.22,

    // elevation: 3,
  },

});