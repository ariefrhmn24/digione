/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Actionsheet, Text, View, useDisclose, Pressable, HStack} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {regexMail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform, Dimensions} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Moment from 'moment';

import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcCalendar from 'assets/svg/ic-calendar.svg';
import {postSetPassword} from 'services/onboard.thunks';

const windowWidth = Dimensions.get('window').width;

function SignupId({
  goToSignupCompleted,
  isLoading,
  date,
  setDate
}) {
  const {t, i18n} = useTranslation();
  const {control, handleSubmit, errors} = useForm();
  const {
    isOpen,
    onOpen,
    onClose
  } = useDisclose();

  const [mode] = useState('date');
  const [show, setShow] = useState(false);
  const [dateFlag, setDateFlag] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    if (Platform.OS === 'android') {
      setDateFlag(true);
      setShow(false);
      onClose();
    } 
    setDate(currentDate);
  };

  const onOpenModal = async () => {
    await onOpen();
    setShow(true);
  };

  const onCloseModal = () => {
    onClose();
    setDateFlag(true);
  };

  return (
    <Box p='5' flex={1} justifyContent={'space-between'} bg='white'>
      <Actionsheet isOpen={isOpen} onClose={onClose} onPress={onClose}>
        <Actionsheet.Content bg='white'>
          <Box width={windowWidth}>
            {show && (<DateTimePicker
              testID='dateTimePicker'
              value={date}
              mode={mode}
              is24Hour={true}
              display='spinner'
              onChange={onChange}
            />)}
          </Box>

          <Button
            onPress={onCloseModal} 
            bg='brand.500' width={windowWidth - 16}
            py={3} size='lg'>
            {t('txtSave')}
          </Button>
        </Actionsheet.Content>
      </Actionsheet>
      <VStack space={10}>
        <Text fontWeight='semibold' fontSize='3xl'>{t('txtIdentity1')}</Text>
        <VStack space={5}>
          <FormControl isRequired isInvalid={'name' in errors}>
            <Controller
              control={control}
              render={({onChange, onBlur, value}) => (
                <Input
                  onBlur={onBlur}
                  placeholder={t('txtNamePlaceholder')}
                  onChangeText={(val) => onChange(val)}
                  value={value}
                />
              )}
              name='name'
              rules={{minLength: 1}}
              defaultValue=''
            />
          </FormControl>
          <Pressable onPress={onOpenModal}>
            <HStack 
              borderWidth={1} 
              borderRadius={10} 
              px='4' py='4' 
              justifyContent={'space-between'} 
              alignItems={'center'}
              borderColor='muted.200'>
              {dateFlag ?
                <Text color='text.500' fontSize='md'>{Moment(date).format('L')}</Text>
                :
                <Text color='muted.400' fontSize='md'>{t('txtBirthdatePlaceholder')}</Text>
              }
              <IcCalendar width='20' height='20' />
            </HStack>
          </Pressable>
        </VStack>
      </VStack>
      <Button
        onPress={handleSubmit(goToSignupCompleted)} 
        isLoading={isLoading} 
        isLoadingText='Submitting'
        bg='brand.500' 
        py={4}
        size='lg'>
        {t('txtContinue')}
      </Button>
      
    </Box>
  );
}

export default SignupId;

SignupId.defaultProps = {
  isLoading: false,
  goToSignupCompleted: () => {},
};

SignupId.propTypes = {
  isLoading: PropTypes.bool,
  goToSignupCompleted: PropTypes.func,
};