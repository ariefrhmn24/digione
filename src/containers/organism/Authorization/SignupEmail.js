/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, ScrollView, Text, View} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {regexMail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform} from 'react-native';

import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IcEyeOpen from 'assets/svg/ic-eye-open.svg';
import {postSetPassword} from 'services/onboard.thunks';

function SignupEmail({
  goToSignupOtp,
  isLoading,
  rdxEmail
}) {
  const {t, i18n} = useTranslation();
  const {control, handleSubmit, errors, watch, setValue} = useForm();

  useEffect(() => {
    async function initialize() {
      setValue('email', rdxEmail, {shouldDirty: true});
    }

    initialize();
  }, []);

  return (
    <Box p='5' flex={1} justifyContent={'space-between'} bg='white'>
      <VStack space={10}>
        <Text fontWeight='semibold' fontSize='3xl'>{t('txtSignupEmail')}</Text>
        <FormControl isRequired isInvalid={'email' in errors}>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <Input
                onBlur={onBlur}
                placeholder={t('txtEmailPlaceholder')}
                onChangeText={(val) => onChange(val)}
                value={value}
              />
            )}
            name='email'
            rules={{
              required: t('txtEmailError'), 
              minLength: 1, 
              pattern: regexMail()
            }}
            defaultValue=''
          />
          {'email' in errors ? 
            <FormControl.ErrorMessage>{t('txtEmailError')}</FormControl.ErrorMessage> 
            :
            <FormControl.HelperText>
              {''}
            </FormControl.HelperText>}
        </FormControl>
      </VStack>
      <Button
        onPress={handleSubmit(goToSignupOtp)} 
        isLoading={isLoading} 
        isLoadingText='Submitting'
        bg='brand.500' 
        py={4}
        size='lg'>
        {t('txtContinue')}
      </Button>
    </Box>
  );
}

export default SignupEmail;

SignupEmail.defaultProps = {
  isLoading: false,
  goToSignupOtp: () => {},
  rdxEmail: ''
};

SignupEmail.propTypes = {
  isLoading: PropTypes.bool,
  goToSignupOtp: PropTypes.func,
  rdxEmail: PropTypes.string
};