/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-bind */
import React, {useState, useEffect} from 'react';
import {Box, VStack, Input, Button, FormControl, Actionsheet, Text, View, useDisclose, Pressable, HStack, Center} from 'native-base';
import {useForm, Controller, Validate} from 'react-hook-form';
import {PropTypes} from 'prop-types';
import {useNavigation, CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {debounce, result} from 'lodash';
import {regexMail} from 'helpers';
import {useSelector, useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Platform, Dimensions} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Moment from 'moment';

import {myToast} from 'components/Toast';
import {AuthContext} from 'routes/context';
import IllustrationCompleted from 'assets/svg/illustration-completed.svg';

function SignupCompleted({
  isLoading,
  goToExplore
}) {
  const {t, i18n} = useTranslation();

  return (
    <Center p='5' flex={1} bg='white'>
      <VStack space='8'>
        <IllustrationCompleted />
        <Text fontSize='3xl' textAlign='center'>{t('txtSignupCompleted1')}</Text>
        <Text textAlign='center'>{t('txtSignupCompleted2')}</Text>
        <Button
          onPress={goToExplore} 
          isLoading={isLoading} 
          isLoadingText='Submitting'
          bg='brand.500' 
          py={4}
          size='lg'>
          {t('txtStart')}
        </Button>
      </VStack>
    </Center>
  );
}

export default SignupCompleted;

SignupCompleted.defaultProps = {
  isLoading: false,
  goToExplore: () => {},
};

SignupCompleted.propTypes = {
  isLoading: PropTypes.bool,
  goToExplore: PropTypes.func,
};