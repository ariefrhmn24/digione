/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const myAssets = [
  'bmp',
  'gif',
  'jpg',
  'jpeg',
  'png',
  'psd',
  'svg',
  'webp',
  'm4v',
  'mov',
  'mp4',
  'mpeg',
  'mpg',
  'webm',
  'aac',
  'aiff',
  'caf',
  'm4a',
  'mp3',
  'wav',
  'html',
  'json',
  'pdf',
  'yaml',
  'yml',
  'otf',
  'ttf',
  'zip',
];

module.exports = {
  resolver: {
    sourceExts: ['jsx', 'js', 'ts', 'tsx', 'json', 'svg'],
    assetExts: myAssets.filter((ext) => ext !== 'svg'),
  },
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
    babelTransformerPath: require.resolve('react-native-svg-transformer'),
  },
};
