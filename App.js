import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import {NativeBaseProvider} from 'native-base';
import codePush from 'react-native-code-push';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';

import Routes from 'routes/navigation';
import store from 'redux/store';
import {colorModeManager, theme} from 'theme/theme';
import {ToastProvider} from 'components/Toast';

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_START,
  updateDialog: {
    appendReleaseDescription: true,
    title: 'a new update is available!'
  },
  installMode: codePush.InstallMode.ON_NEXT_RESUME,
};

function App () {
  const config = {
    dependencies: {
      'linear-gradient': require('react-native-linear-gradient').default,
    },
    // strictMode: 'warn',
  };
  
  let persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NativeBaseProvider theme={theme} config={config}>
          <ToastProvider>
            <Routes />
          </ToastProvider>
        </NativeBaseProvider>
      </PersistGate>
    </Provider>
  );
}

export default codePush(codePushOptions)(App);